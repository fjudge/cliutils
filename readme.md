# CliUtils

The CliUtils package is a generic command line parsing utility. 
The goal of the project is to limit the repetative tasks that one has to perform 
when creating an application that makes use of the command line.

## Usage

The simplest way to use the library is as a command line argument parser:

```csharp

    ICliParser parser = CliUtils.CreateParser();
    IParseResult result = parser.Parse(args);

``` 

This can then be used to parse the command arguments into an array of 
`ICliArgument`, which can in turn be interrogated. 

`CliUtils` has some opinions about the the format of commands and command 
switches, it assumes that:

- Long switch names start with a dounble dash (--), 
- and that short switch names start with a single dash (-)

But this siple usage makes no use of the core features of the library, such
as a built-in help system writer. A basig usage example of this would be:

```csharp

    ICliConfiguration config = CliUtils
        .CreateConfiguration()
        .Global
        .AddSwitch("Verbose", "v")
        .SetHelpText("Indicates whether the application should emit verbose output.")
        .Command
        .Config;
    ICliParser parser = CliUtils.CreateParser(config);
    IParseResult result = parser.Parse(args);
```

In this case the `IParseResult` contains a additional information about what 
was parsed from the supplied `args`.

The bigest advantage of this approach is that the `config` object can be passed 
to the `CliUtils` which will automatically produce nicely formatted help text:

```csharp

    CliUtils.WriteHelp(config);

```

Which produces the following output:

```text

    Global Switches:
      --NoLogo, -nl             This switch indicates whether the automatic outputting of application
                                information should be suppressed.
      --Verbose, -v             Indicates whether the application should emit verbose output.


```

**N.B.** The `NoLogo` option is one of a few built-in global commands.
