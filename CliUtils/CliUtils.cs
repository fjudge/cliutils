﻿using FJSolutions.CommandLine.Implementation;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace FJSolutions.CommandLine
{
    /// <summary>
    /// This is the factory class for using the CliUtils library. It contains all the methods needed for setting up and running command line applications.
    /// </summary>
    public static class CliUtils
    {
        /// <summary>
        /// Creates an <see cref="ICliConfiguration"/> object for configuring the parsing of command line arguments in an application.
        /// </summary>
        /// <returns>An <see cref="ICliConfiguration"/> instance.</returns>
        public static ICliConfiguration CreateConfiguration()
        {
            return new CliConfiguration()
                .SetAppInfo();
        }

        /// <summary>
        /// Loads configuration information from an XML document into a new <see cref="ICliConfiguration" /> object.
        /// </summary>
        /// <param name="xmlTextReader">The XML text reader.</param>
        /// <returns>An <see cref="ICliConfiguration" /> instance.</returns>
        public static ICliConfiguration LoadConfiguration(TextReader xmlTextReader)
        {
            if (xmlTextReader == null)
                throw new ArgumentException("xmlTextReader");

            var loader = new XmlConfigurationLoader()
                .LoadConfig(xmlTextReader)
                .BuildConfig();

            return loader.Config;
        }

        /// <summary>
        /// Loads configuration information from an XML document into a new <see cref="ICliConfiguration"/> object.
        /// </summary>
        /// <param name="configurationXml">The configuration XML text.</param>
        /// <returns>An <see cref="ICliConfiguration"/> instance</returns>
        public static ICliConfiguration LoadConfiguration(string configurationXml)
        {
            if (string.IsNullOrWhiteSpace(configurationXml))
                throw new ArgumentException("configurationXml");

            var stream = new StringReader(configurationXml);

            return CliUtils.LoadConfiguration(stream);
        }

        /// <summary>
        /// Creates a <see cref="ICliParser" /> from the supplied configuration.
        /// <para>(If no configuration is supplied then the help features cannot be used, and only simple command line parsing is performed)</para>
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns>A <see cref="ICliParser" /> for parsing command line arguments.</returns>
        public static ICliParser CreateParser(ICliConfiguration config = null)
        {
            var parser = new CliParser { Config = (CliConfiguration)config };

            return parser;
        }

        /// <summary>
        /// Outputs the contents of an invalid parse result.
        /// </summary>
        /// <param name="config">The configuration object.</param>
        /// <param name="results">The <see cref="IParseResult" />.</param>
        /// <param name="showHelp">if set to <c>true</c> help will be shown after the error message.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="options">A <see cref="HelpWriterOptions" /> overload.</param>
        /// <exception cref="ArgumentException">config or results </exception>
        public static void WriteInvalidParse(ICliConfiguration config, IParseResult results, bool showHelp = true, TextWriter stream = null, HelpWriterOptions options = null)
        {
            if (config == null)
                throw new ArgumentException("config");
            if (results == null)
                throw new ArgumentException("results");

            var writer = (HelpWriter)CliUtils.WriteAppTitle(config, stream, options);

            if (!results.HasBeenValidated || results.ValidationResult == null)
                writer.WriteError("There was an error parsing the command line arguments!");
            else
            {
                var messages = results.ValidationResult.Messages;
                if (messages.Length == 1)
                    writer.WriteError(messages[0]);
                else
                {
                    writer.WriteError("Errors:");
                    foreach (var msg in messages)
                        writer.WriteError("- " + msg);
                }
            }

            if (showHelp)
            {
                writer.WriteLine();
                writer.WriteShortHelp((CliConfiguration)config);
            }
        }

        /// <summary>
        /// Outputs the application title.
        /// </summary>
        /// <param name="config">The configuration object.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="options">A <see cref="HelpWriterOptions" /> overload.</param>
        /// <returns>A <see cref="HelpWriter"/> for external use.</returns>
        /// <exception cref="ArgumentException">config</exception>
        public static IHelpWriter WriteAppTitle(ICliConfiguration config, TextWriter stream = null, HelpWriterOptions options = null)
        {
            if (config == null)
                throw new ArgumentException("config");

            var writer = CliUtils.CreateHelpWriter((CliConfiguration)config, stream, options);

            if (config.AppInfo != null)
                writer.WriteAppInfo(config.AppInfo);

            return writer;
        }

        /// <summary>
        /// Writes help information in the <paramref name="config" /> into the output <paramref name="stream" />, which defaults to <c>Console.Out</c>.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="options">A <see cref="HelpWriterOptions"/> overload.</param>
        /// <exception cref="ArgumentException">stream</exception>
        public static void WriteHelp(ICliConfiguration config, TextWriter stream = null, HelpWriterOptions options = null)
        {
            if (config == null)
                throw new ArgumentException("config");

            var cliConfig = (CliConfiguration)config;
            var writer = CliUtils.CreateHelpWriter(cliConfig, stream, options);

            // No Logo
            if (config.AppInfo != null && cliConfig.Global.GetParsedSwitchArgumnent("NoLogo").IsValueNullOrWhiteSpace())
                writer.WriteAppInfo(config.AppInfo);

            // Help on a specific command
            if (cliConfig.Help.HasValueSet())
            {
                CommandConfig command = null;
                var commandName = cliConfig.Help.ParsedValue.Value;

                // Find the command and issue and error if it can't be found
                if (String.Equals(commandName, cliConfig.Global.Name, StringComparison.CurrentCultureIgnoreCase))
                    command = cliConfig.Global;
                else if (String.Equals(commandName, cliConfig.Help.Name, StringComparison.CurrentCultureIgnoreCase))
                    command = cliConfig.Help;
                else
                    command = (CommandConfig)cliConfig.Commands.FirstOrDefault(c => ((CommandConfig)c).IsCommandName(commandName));

                if (command != null)
                {
                    writer.WriteManHelp(command, new StringBuilder());
                    return;
                }
            }

            // Long help output
            if (!cliConfig.Help.GetParsedSwitchArgumnent("Manual").IsNull())
                writer.WriteManHelp(cliConfig);
            else
                // Short help
                writer.WriteShortHelp(cliConfig);

            writer.Stream.Dispose();
        }

        private static HelpWriter CreateHelpWriter(CliConfiguration config, TextWriter stream, HelpWriterOptions options)
        {
            HelpWriter writer = null;

            if (options == null)
                options = new HelpWriterOptions();

            // Check for a null stream
            if (stream == null)
            {
                // Check if there's an output file supplied in the config
                var outputFile = config.Help.GetParsedSwitchArgumnent("File").GetValue();
                if (!string.IsNullOrWhiteSpace(outputFile))
                {
                    var dir = Path.GetDirectoryName(outputFile);
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    stream = new StreamWriter(outputFile, false);
                    writer = new HelpWriter(stream, false, options);
                }
                else
                {
                    // Try to set it to the console
                    try
                    {
                        if (Console.Out != null)
                        {
                            writer = new HelpWriter(Console.Out, true, options);
                            options.DefaultColour = Console.ForegroundColor;

                            try
                            {
                                options.LineWidth = Console.BufferWidth;
                            }
                            catch (Exception) { }
                        }
                        else
                            throw new ArgumentException("stream");
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException("stream", ex);
                    }
                }
            }
            else
                writer = new HelpWriter(stream, false, options);

            return writer;
        }
    }
}
