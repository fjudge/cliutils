﻿using FJSolutions.CommandLine.Implementation;
using System;
using System.IO;
using System.Xml.Linq;

namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Contains extension methods for <see cref="ICliConfiguration"/> objects.
    /// </summary>
    public static class CliConfigExtensions
    {
        private static readonly XNamespace XMLNS = XNamespace.Get("http://fjsolutions.co.za/CliUtilsConfig.xsd");

        /// <summary>
        /// Saves the <see cref="ICliConfiguration"/> to an XML file.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="fileName">Name of the XML file to save the <see cref="ICliConfiguration"/> into.</param>
        /// <exception cref="ArgumentException">fileName</exception>
        public static void SaveXml(this ICliConfiguration config, string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("fileName");

            var path = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            SaveXml(config, new StreamWriter(fileName, false));
        }

        /// <summary>
        /// Saves the <see cref="ICliConfiguration"/> to XML.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="stream">The stream to save the <see cref="ICliConfiguration"/> into.</param>
        /// <exception cref="ArgumentException">config or stream</exception>
        public static void SaveXml(this ICliConfiguration config, TextWriter stream)
        {
            if (config == null)
                throw new ArgumentException("config");
            if (stream == null)
                throw new ArgumentException("stream");

            var cliConfig = (CliConfiguration)config;

            var xDoc = new XDocument();
            xDoc.Declaration = new XDeclaration("1.0", "utf-8", "yes");
            var root = new XElement(XMLNS + "config");

            // AppInfo
            if (config.AppInfo != null)
            {
                var appInfo = (AppInfo)config.AppInfo;

                var appElement = CliConfigExtensions.AddElement(root, "appInfo");
                CliConfigExtensions.AddElement(appElement, "name", appInfo.Name);
                if (!string.IsNullOrWhiteSpace(appInfo.Description))
                    CliConfigExtensions.AddElement(appElement, "description", appInfo.Description);
                if (!string.IsNullOrWhiteSpace(appInfo.Company))
                    CliConfigExtensions.AddElement(appElement, "company", appInfo.Company);
                if (!string.IsNullOrWhiteSpace(appInfo.Copyright))
                    CliConfigExtensions.AddElement(appElement, "copyright", appInfo.Copyright);
                if (!string.IsNullOrWhiteSpace(appInfo.VersionNumber))
                    CliConfigExtensions.AddElement(appElement, "versionNumber", appInfo.VersionNumber);
                if (!string.IsNullOrWhiteSpace(appInfo.VersionName))
                    CliConfigExtensions.AddElement(appElement, "versionName", appInfo.VersionName);
            }

            // Commands
            foreach (CommandConfig command in cliConfig.Commands)
            {
                CliConfigExtensions.AddCommand(root, command);
            }

            // HelpInfo
            if (cliConfig.HelpInfo != null)
                CliConfigExtensions.AddHelpInfo(root, cliConfig.HelpInfo);

            xDoc.Add(root);

            using (stream)
            {
                xDoc.Save(stream);
            }
        }

        private static void AddHelpInfo(XElement parent, IHelpInfo helpInfo)
        {
            var helpElement = CliConfigExtensions.AddElement(parent, "helpInfo");

            if (!string.IsNullOrWhiteSpace(helpInfo.Description))
                CliConfigExtensions.AddElement(helpElement, "description", helpInfo.Description, true);

            if (helpInfo.LongDescription != null && helpInfo.LongDescription.Length > 0 && !string.IsNullOrWhiteSpace(helpInfo.LongDescription[0]))
                CliConfigExtensions.AddElement(helpElement, "longDescription", string.Join(Environment.NewLine + Environment.NewLine, helpInfo.LongDescription), true);

            if (!string.IsNullOrWhiteSpace(helpInfo.ExampleUsage))
                CliConfigExtensions.AddElement(helpElement, "exampleUsage", helpInfo.ExampleUsage);
        }

        private static void AddCommand(XElement parent, CommandConfig command)
        {
            var cmdElement = CliConfigExtensions.AddElement(parent, "command");
            cmdElement.Add(new XAttribute("name", command.Name));
            if (!string.IsNullOrWhiteSpace(command.ShortName))
                cmdElement.Add(new XAttribute("shortName", command.ShortName));

            if (!string.IsNullOrWhiteSpace(command.ValueHelpName))
            {
                var vhnElement = CliConfigExtensions.AddElement(cmdElement, "valueHelpName", command.ValueHelpName);
                vhnElement.Add(new XAttribute("required", command.IsValueRequired.ToString().ToLowerInvariant()));
                vhnElement.Add(new XAttribute("valueType", command.CommandValueType.ToString()));
            }

            if (command.Switches.Length > 0)
            {
                var swElement = CliConfigExtensions.AddElement(cmdElement, "switches");

                foreach (var sw in command.Switches)
                {
                    CliConfigExtensions.AddSwitch(swElement, sw);
                }
            }

            CliConfigExtensions.AddHelpInfo(cmdElement, command.HelpInfo);
        }

        private static void AddSwitch(XElement parent, SwitchConfig sw)
        {
            var swElement = CliConfigExtensions.AddElement(parent, "switch");
            swElement.Add(new XAttribute("name", sw.Name));
            if (!string.IsNullOrWhiteSpace(sw.ShortName))
                swElement.Add(new XAttribute("shortName", sw.ShortName));

            if (!string.IsNullOrWhiteSpace(sw.ValueHelpName))
            {
                var vhnElement = CliConfigExtensions.AddElement(swElement, "valueHelpName", sw.ValueHelpName);
                vhnElement.Add(new XAttribute("required", sw.IsValueRequired.ToString().ToLowerInvariant()));
            }

            CliConfigExtensions.AddHelpInfo(swElement, sw.HelpInfo);
        }

        private static XElement AddElement(XElement parent, string elementName, string text = null, bool textAsCdata = false)
        {
            var element = new XElement(XMLNS + elementName);

            if (!string.IsNullOrWhiteSpace(text))
            {
                if (textAsCdata)
                {
                    var cdata = new XCData(text);
                    element.Add(cdata);
                }
                else
                    element.Value = text;
            }

            parent.Add(element);

            return element;
        }
    }
}
