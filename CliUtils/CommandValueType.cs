﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Lists the types of command values
    /// </summary>
    public enum CommandValueType
    {
        /// <summary>
        /// The command has no values.
        /// </summary>
        None = 0,

        /// <summary>
        /// The command has a single value
        /// </summary>
        Single = 1,

        /// <summary>
        /// The command has multiple values
        /// </summary>
        Multiple = 2
    }
}
