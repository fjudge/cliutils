﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for objects that set <see cref="IHelpInfo"/>
    /// </summary>
    public interface IHelpInfoConfig<TReturn>
    {
        /// <summary>
        /// Sets the help description.
        /// </summary>
        /// <param name="helpDescription">The help description.</param>
        /// <returns>A <see cref="TReturn" /> for fluid configuration.</returns>
        TReturn SetHelpDescription(string helpDescription);

        /// <summary>
        /// Sets the long help description.
        /// </summary>
        /// <param name="longHelp">The long help.</param>
        /// <returns>A <see cref="TReturn" /> for fluid configuration.</returns>
        TReturn SetHelpLongDescription(string longHelp);

        /// <summary>
        /// Sets the help usage / example text for the application.
        /// </summary>
        /// <param name="helpUsage">The help usage.</param>
        /// <returns>A <see cref="TReturn" /> for fluid configuration.</returns>
        TReturn SetHelpExampleUsage(string helpUsage);
    }
}
