﻿using System;

namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for configuring <see cref="CliUtils"/>;
    /// </summary>
    public interface ICliConfiguration : IHelpInfoConfig<ICliConfiguration>
    {
        /// <summary>
        /// Gets the application information for this configuration.
        /// </summary>
        IAppInfo AppInfo { get; }

        /// <summary>
        /// Gets the global <see cref="ICommandConfig"/>.
        /// </summary>
        ICommandConfig Global { get; }

        /// <summary>
        /// Adds the command.
        /// </summary>
        /// <param name="commandName">Name of the command.</param>
        /// <returns>A <see cref="ICommandConfig"/> for fluid configuration.</returns>
        ICommandConfig AddCommand(string commandName);

        /// <summary>
        /// Adds the command.
        /// </summary>
        /// <param name="commandName">Name of the command.</param>
        /// <param name="shortName">The short name.</param>
        /// <returns>A <see cref="ICommandConfig" /> for fluid configuration.</returns>
        ICommandConfig AddCommand(string commandName, string shortName);

        /// <summary>
        /// Sets the application information for the <see cref="System.Reflection.Assembly"/> that the supplied <see cref="Type"/> belongs to.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>A <see cref="ICliConfiguration" /> for fluid configuration.</returns>
        ICliConfiguration SetAppInfo(Type type);

        /// <summary>
        /// Sets the application information from the entry <see cref="System.Reflection.Assembly"/>.
        /// </summary>
        /// <returns>A <see cref="ICliConfiguration" /> for fluid configuration.</returns>
        ICliConfiguration SetAppInfo();

        /// <summary>
        /// Gets the command selected in the parsed arguments.
        /// </summary>
        /// <returns>The <see cref="IParsedCommand"/> entered in the parsed arguments; otherwise <c>null</c>.</returns>
        //IParsedCommand GetSelectedCommand();
    }
}
