﻿namespace FJSolutions.CommandLine
{
    public interface ICliArgument
    {
        /// <summary>
        /// Gets the normalised name of the argument.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the raw text of the command.
        /// </summary>
        string Raw { get; }

        /// <summary>
        /// Gets the position of the argument in the array.
        /// </summary>
        int Position { get; }

        /// <summary>
        /// Gets the type of the argument.
        /// </summary>
        CliArgumentType ArgumentType { get; }

        /// <summary>
        /// Gets the value of the <see cref="ICliArgument"/>.
        /// </summary>
        string Value { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ICliArgument"/> has a value.
        /// </summary>
        bool HasValue { get; }
    }
}
