﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines an interface for help text information.
    /// </summary>
    public interface IHelpInfo
    {
        /// <summary>
        /// Gets a sort description paragraph.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the array of long description paragraphs.
        /// </summary>
        string[] LongDescription { get; }

        /// <summary>
        /// Gets any usage example text.
        /// </summary>
        string ExampleUsage { get; }
    }
}
