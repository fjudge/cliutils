﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for a parsed command
    /// </summary>
    public interface IParsedCommand
    {
        /// <summary>
        /// Gets the canonical name of the command.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the parsed value of the command, if there is one.
        /// </summary>
        string Value { get; }

        /// <summary>
        /// Determines whether a switch exists and has been set.
        /// </summary>
        /// <param name="switchName">Name of the switch.</param>
        /// <returns>
        ///   <c>true</c> if the switch is present and has been set; otherwise, <c>false</c>.
        /// </returns>
        bool IsSwitchSet(string switchName);

        /// <summary>
        /// Tries to get a <see cref="IParsedSwitch"/> that has been set, by its name.
        /// </summary>
        /// <param name="switchName">Name of the switch.</param>
        /// <param name="switch">The switch.</param>
        /// <returns><c>true</c> if the <see cref="IParsedSwitch"/> was found and set; otherwise <c>false</c>.</returns>
        bool TryGetSetSwitch(string switchName, out IParsedSwitch @switch);
    }
}
