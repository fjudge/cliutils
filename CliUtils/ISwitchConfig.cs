﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the configuration interface for switches.
    /// </summary>
    /// <seealso cref="FJSolutions.CommandLine.IHelpInfo" />
    public interface ISwitchConfig : IHelpInfoConfig<ISwitchConfig>
    {
        /// <summary>
        /// Gets the canonical name of this <see cref="ISwitchConfig"/>.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the short name for the <see cref="ISwitchConfig"/>.
        /// </summary>
        string ShortName { get; }

        /// <summary>
        /// Gets the reverence to the parent <see cref="ICommandConfig"/>.
        /// </summary>
        ICommandConfig Command { get; }

        /// <summary>
        /// Gets a value indicating whether this command has a required value.
        /// </summary>
        bool IsValueRequired { get; }

        /// <summary>
        /// Sets the short name of the <see cref="ICliArgument"/>.
        /// </summary>
        /// <param name="shortName">The short name.</param>
        /// <returns>This <see cref="ISwitchConfig"/> for chaining purposes</returns>
        ISwitchConfig SetShortName(string shortName);

        /// <summary>
        /// Sets the name of the value to display in the help text.
        /// </summary>
        /// <param name="valueNameHelp">Name of the value.</param>
        /// <param name="isValueRequired">if set to <c>true</c> [is value required].</param>
        /// <returns>This <see cref="ISwitchConfig" /> for chaining purposes</returns>
        ISwitchConfig SetValueHelpName(string valueNameHelp, bool isValueRequired = true);

        /// <summary>
        /// Sets the is value required.
        /// </summary>
        /// <param name="isValueRequired">if set to <c>true</c> then a value is required for this .</param>
        /// <returns>This <see cref="ISwitchConfig" /> for chaining purposes</returns>
        ISwitchConfig SetIsValueRequired(bool isValueRequired = true);
    }
}
