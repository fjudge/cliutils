﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for a parsed command switch.
    /// </summary>
    public interface IParsedSwitch
    {
        /// <summary>
        /// Gets the canonical name of the command.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the parsed value of the switch, if one was set.
        /// </summary>
        string Value { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IParsedSwitch"/> has been set in in a parse.
        /// </summary>
        bool IsSet { get; }
    }
}
