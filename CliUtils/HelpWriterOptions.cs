﻿using System;

namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Represents an object that contains various settings which the <see cref="IHelpInfo"/> uses.
    /// </summary>
    public class HelpWriterOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpWriterOptions"/> class.
        /// </summary>
        public HelpWriterOptions()
        {
            this.DefaultColour = ConsoleColor.White;
            this.ErrorColour = ConsoleColor.DarkRed;
            this.SuccessColour = ConsoleColor.DarkGreen;
            this.LineWidth = 80;
            this.CommandWidth = 28;
            this.CommandSwitchIndent = 2;
        }

        /// <summary>
        /// Gets or sets the width of the output stream's line, before inserting an end of line.
        /// </summary>
        public int LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default forecolour to use in the console.
        /// </summary>
        public ConsoleColor DefaultColour { get; set; }

        /// <summary>
        /// Gets or sets the forecolour to use for error messages in the console.
        /// </summary>
        public ConsoleColor ErrorColour { get; set; }

        /// <summary>
        /// Gets or sets the forecolour to use for success messages in the console.
        /// </summary>
        public ConsoleColor SuccessColour { get; set; }

        /// <summary>
        /// Gets or sets the number of characters for the command column in the output.
        /// </summary>
        public int CommandWidth { get; set; }

        /// <summary>
        /// Gets or sets the indent for command switches in the command column of the output.
        /// </summary>
        public int CommandSwitchIndent { get; set; }
    }
}
