﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for a command-line arguments parser.
    /// </summary>
    public interface ICliParser
    {
        /// <summary>
        /// Parses the specified command-line arguments, after converting the string to a parsed array.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="autoValidate">if set to <c>true</c> then automatically validate the results against the config definition.</param>
        /// <returns>An <see cref="IParseResult" />.</returns>
        IParseResult Parse(string args, bool autoValidate = true);

        /// <summary>
        /// Parses the specified command-line arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="autoValidate">if set to <c>true</c> then automatically validate the results against the config definition.</param>
        /// <returns>An <see cref="IParseResult"/>.</returns>
        IParseResult Parse(string[] args, bool autoValidate = true);

        /// <summary>
        /// Validates the specified result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="config">The configuration.</param>
        /// <returns>A <see cref="IValidationResult" />.</returns>
        IValidationResult Validate(IParseResult result, ICliConfiguration config);
    }
}
