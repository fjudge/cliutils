﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Lists the types of <see cref="IParseResult"/>.
    /// </summary>
    public enum ParseResultType
    {
        /// <summary>
        /// The result of parsing the arguments was an empty array of arguments.
        /// </summary>
        Empty = 0,

        /// <summary>
        /// The first argument was a command name.
        /// </summary>
        Command = 1,

        /// <summary>
        /// The reult of parsing was a help request
        /// </summary>
        Help = 2,

        /// <summary>
        /// The parse reult contains switches only and no command.
        /// </summary>
        SwitchesOnly = 3,

        /// <summary>
        /// The parse result was not valid after validating the arguments against the configured definitions.
        /// </summary>
        Invalid = 4
    }
}
