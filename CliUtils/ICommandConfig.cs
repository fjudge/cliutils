﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for configuring a command.
    /// </summary>
    public interface ICommandConfig : IHelpInfoConfig<ICommandConfig>
    {
        /// <summary>
        /// Gets the canonical name of this <see cref="ICommandConfig"/>.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the short name for the <see cref="ICommandConfig"/>.
        /// </summary>
        string ShortName { get; }

        /// <summary>
        /// Gets a reference to the parent configuration.
        /// </summary>
        ICliConfiguration Config { get; }

        /// <summary>
        /// Gets a value indicating the type of values that the  <see cref="ICliArgument"/> can receive.
        /// </summary>
        CommandValueType CommandValueType { get; }

        /// <summary>
        /// Gets a value indicating whether this command has a required value.
        /// </summary>
        bool IsValueRequired { get; }

        /// <summary>
        /// Gets the name of the value to be displayed in the help text.
        /// </summary>
        string ValueHelpName { get; }

        /// <summary>
        /// Sets the short name of the <see cref="ICliArgument"/>.
        /// </summary>
        /// <param name="shortName">The short name.</param>
        /// <returns>This <see cref="ICommandConfig"/> for chaining purposes</returns>
        ICommandConfig SetShortName(string shortName);

        /// <summary>
        /// Sets the is value required.
        /// </summary>
        /// <param name="isValueRequired">if set to <c>true</c> then a value is required for this .</param>
        /// <returns>This <see cref="ICommandConfig" /> for chaining purposes</returns>
        ICommandConfig SetIsValueRequired(bool isValueRequired = true);

        /// <summary>
        /// Sets the number of values that the <see cref="ICliArgument" /> can receive.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="valueHelpName">Name of the value parameter to be displayed in the help text.</param>
        /// <returns>This <see cref="ICommandConfig" /> for chaining purposes</returns>
        ICommandConfig SetValueType(CommandValueType valueType, string valueHelpName = null);

        /// <summary>
        /// Adds a new switch definition for the <see cref="ICommandConfig"/>.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A <see cref="ISwitchConfig"/></returns>
        /// <exception cref="System.ArgumentException">name</exception>
        ISwitchConfig AddSwitch(string name);

        /// <summary>
        /// Adds a new switch definition for the <see cref="ICliArgument" />.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="shortName">The short name.</param>
        /// <returns>A <see cref="ISwitchConfig" /></returns>
        /// <exception cref="System.ArgumentException">name</exception>
        ISwitchConfig AddSwitch(string name, string shortName);
    }
}
