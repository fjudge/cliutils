﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for applicaiton information.
    /// </summary>
    public interface IAppInfo
    {
        /// <summary>
        /// Gets and sets the name of the application.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets and sets the version number of the application.
        /// </summary>
        string VersionNumber { get; set; }

        /// <summary>
        /// Gets and sets the symanic version of the application.
        /// </summary>
        string VersionName { get; set; }

        /// <summary>
        /// Gets and sets a description of the application.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets and sets company information for the application.
        /// </summary>
        string Company { get; set; }

        /// <summary>
        /// Gets and sets copyright information about the application.
        /// </summary>
        string Copyright { get; set; }
    }
}
