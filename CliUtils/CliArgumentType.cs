﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Lists the Command Argument types
    /// </summary>
    public enum CliArgumentType
    {
        /// <summary>
        /// A command argument.
        /// </summary>
        Command = 0,

        /// <summary>
        /// A flagged switch without a property.
        /// </summary>
        Switch = 1
    }
}
