﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represents an object that provides configuration information for an <see cref="ICliArgument"/>.
    /// </summary>
    public class CommandConfig : IParsedCommand, ICommandConfig
    {
        // Fields
        private List<SwitchConfig> _switches;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandConfig" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parent">The parent.</param>
        internal CommandConfig(string name, ICliConfiguration parent)
        {
            this.HelpInfo = new HelpInfo();
            name = name.TrimArgument();
            this._switches = new List<SwitchConfig>();
            this.Name = name;
            this.Config = parent;
        }

        public ICliConfiguration Config { get; private set; }

        public HelpInfo HelpInfo { get; set; }

        public string Name { get; private set; }

        public CommandValueType CommandValueType { get; private set; }

        public bool IsValueRequired { get; private set; }

        public string ValueHelpName { get; private set; }

        /// <summary>
        /// Gets the short name for the <see cref="ICliArgument"/>.
        /// </summary>
        public string ShortName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this command has been selected on the command line.
        /// </summary>
        public bool IsSelected
        {
            get { return this.ParsedValue != null; }
        }

        /// <summary>
        /// Gets the array of switches for the <see cref="ICliArgument"/>.
        /// </summary>
        public SwitchConfig[] Switches
        {
            get { return this._switches.ToArray(); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has switches.
        /// </summary>
        public bool HasSwitches
        {
            get { return this._switches.Count > 0; }
        }

        /// <summary>
        /// Gets the <see cref="SwitchConfig"/> with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A <see cref="IParsedSwitch"/>if found; otherwise <c>null</c>.</returns>
        public IParsedSwitch this[string name]
        {
            get { return this._switches.SingleOrDefault(s => s.IsSwtichName(name)); }
        }

        /// <summary>
        /// Tries to get a <see cref="IParsedSwitch"/> that has been set, by its name.
        /// </summary>
        /// <param name="switchName">Name of the switch.</param>
        /// <param name="switch">The switch.</param>
        /// <returns><c>true</c> if the <see cref="IParsedSwitch"/> was found and set; otherwise <c>false</c>.</returns>
        public bool TryGetSetSwitch(string switchName, out IParsedSwitch @switch)
        {
            @switch = this[switchName];
            if (@switch == null)
                return false;

            if (!@switch.IsSet)
                @switch = null;

            return @switch != null;
        }

        /// <summary>
        /// Determines whether a switch exists and has been set.
        /// </summary>
        /// <param name="switchName">Name of the switch.</param>
        /// <returns>
        ///   <c>true</c> if the switch is present and has been set; otherwise, <c>false</c>.
        /// </returns>
        public bool IsSwitchSet(string switchName)
        {
            var sw = this[switchName];
            if (sw == null)
                return false;

            return sw.IsSet;
        }

        /// <summary>
        /// Gets the <see cref="ICliArgument"/> for this switch if it was parsed from the command line.
        /// </summary>
        public ICliArgument ParsedValue { get; internal set; }

        /// <summary>
        /// Gets the parsed value of the command, if there is one.
        /// </summary>
        public string Value
        {
            get { return this.ParsedValue == null ? string.Empty : this.ParsedValue.Value; }
        }
        public ISwitchConfig AddSwitch(string name)
        {
            return this.AddSwitch(name, null);
        }

        public ISwitchConfig AddSwitch(string name, string shortName)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("name");

            var cliSwitch = new SwitchConfig(name, this);
            cliSwitch.SetShortName(shortName);

            this._switches.Add(cliSwitch);

            return cliSwitch;
        }

        public ICommandConfig SetShortName(string shortName)
        {
            this.ShortName = shortName.TrimArgument();

            return this;
        }

        public ICommandConfig SetHelpDescription(string helpText)
        {
            this.HelpInfo.SetHelpDescription(helpText);
            return this;
        }

        public ICommandConfig SetHelpLongDescription(string helpText)
        {
            this.HelpInfo.SetHelpLongDescription(helpText);
            return this;
        }

        public ICommandConfig SetHelpExampleUsage(string helpText)
        {
            this.HelpInfo.SetHelpExampleUsage(helpText);
            return this;
        }

        public ICommandConfig SetValueType(CommandValueType valueType, string valueHelpName = null)
        {
            this.CommandValueType = valueType;

            if (!string.IsNullOrWhiteSpace(valueHelpName))
            {
                this.ValueHelpName = valueHelpName.Trim();
                this.IsValueRequired = true;
            }

            return this;
        }

        /// <summary>
        /// Sets the is value required.
        /// </summary>
        /// <param name="isValueRequired">if set to <c>true</c> then a value is required for this .</param>
        /// <returns>This <see cref="ICommandConfig" /> for chaining purposes</returns>
        public ICommandConfig SetIsValueRequired(bool isValueRequired = true)
        {
            this.IsValueRequired = isValueRequired;

            return this;
        }

        /// <summary>
        /// Gets the parsed argument for the supplied switch argument name, if present.
        /// </summary>
        /// <param name="argumentName">Name of the switch.</param>
        /// <returns>An <see cref="ICliArgument"/> if the switch exists and the argument was parsed from the command line.</returns>
        public ICliArgument GetParsedSwitchArgumnent(string argumentName)
        {
            var cliSwitch = this._switches
                .SingleOrDefault(s => s.Name.Equals(argumentName, StringComparison.CurrentCultureIgnoreCase) || s.ShortName.Equals(argumentName, StringComparison.CurrentCultureIgnoreCase));

            if (cliSwitch == null)
                return null;

            return cliSwitch.ParsedValue;
        }

        /// <summary>
        /// Returns a value indicating whether this command has been parsed and has a value.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [has value set]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasValueSet()
        {
            if (this.ParsedValue == null)
                return false;

            return !string.IsNullOrWhiteSpace(this.ParsedValue.Value);
        }
    }
}
