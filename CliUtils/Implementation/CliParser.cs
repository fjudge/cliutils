﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represets a commandline argument parser.
    /// </summary>
    /// <seealso cref="FJSolutions.CommandLine.ICliParser" />
    public class CliParser : ICliParser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CliParser"/> class.
        /// </summary>
        internal CliParser()
        {
        }

        /// <summary>
        /// Gets or sets a reference to the <see cref="ICliConfiguration"/> object.
        /// </summary>
        public CliConfiguration Config { get; set; }

        /// <summary>
        /// Tokenizes a string into an array of arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>A <see cref="System.String[]"/> of arguments.</returns>
        public string[] Tokenize(string args)
        {
            if (string.IsNullOrWhiteSpace(args))
                return new string[0];

            var tokens = new List<string>();
            var sb = new StringBuilder();
            var isEscaped = false;

            foreach (var c in args)
            {
                if (c.Equals('"'))
                {
                    isEscaped = !isEscaped;
                    continue;
                }

                if (c.Equals(' ') && !isEscaped)
                {
                    if (sb.Length > 0)
                        tokens.Add(sb.ToString());

                    sb.Clear();
                    continue;
                }

                sb.Append(c);
            }

            if (sb.Length > 0)
                tokens.Add(sb.ToString());


            return tokens.ToArray();
        }

        public IParseResult Parse(string args, bool autoValidate = true)
        {
            return this.Parse(this.Tokenize(args), autoValidate);
        }

        public IParseResult Parse(string[] args, bool autoValidate = true)
        {
            var result = new ParseResult();
            var list = new List<CliArgument>();
            string propertyName;

            for (int i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                var cliArg = new CliArgument { Raw = arg, Position = i };

                // Check the argument type
                if (arg.StartsWith("--", System.StringComparison.CurrentCulture))
                {
                    propertyName = arg.Substring(2);
                    cliArg.ArgumentType = CliArgumentType.Switch;
                }
                else if (arg.StartsWith("-", System.StringComparison.CurrentCulture))
                {
                    propertyName = arg.Substring(1);
                    cliArg.ArgumentType = CliArgumentType.Switch;
                }
                else
                {
                    propertyName = arg;
                    cliArg.ArgumentType = CliArgumentType.Command;
                    result.ResultType = ParseResultType.Command;
                }

                cliArg.Name = propertyName;

                // Check for a value for the argument
                if (i + 1 < args.Length)
                {
                    arg = args[i + 1];
                    if (!arg.StartsWith("-", System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        cliArg.Value = arg;
                        i += 1;
                    }
                }

                list.Add(cliArg);
            }

            result.Arguments = list.ToArray();

            if (result.Arguments.Length > 0)
            {
                if (this.Config != null && ((CliArgument)result.Arguments[0]).IsCommandArgument(this.Config.Help))
                    result.ResultType = ParseResultType.Help;
                else if (result.Arguments[0].ArgumentType == CliArgumentType.Command)
                    result.ResultType = ParseResultType.Command;
                else
                    result.ResultType = ParseResultType.SwitchesOnly;
            }

            if (autoValidate && this.Config != null)
                this.Validate(result, this.Config);

            return result;
        }

        public IValidationResult Validate(IParseResult parseResult, ICliConfiguration cliConfig)
        {
            if (parseResult == null)
                throw new ArgumentException("parseResult");
            if (cliConfig == null)
                throw new ArgumentException("cliConfig");

            var validationResult = new ValidationResult();
            var config = (CliConfiguration)cliConfig;

            if (parseResult.ResultType == ParseResultType.Command)
            {
                // Get the command definition
                var command = parseResult.Arguments[0];
                var switches = parseResult.Arguments.Skip(1).ToArray();
                var commandDefinition = (CommandConfig)config.Commands.SingleOrDefault(c => ((CommandConfig)c).Name.Equals(command.Name, StringComparison.CurrentCultureIgnoreCase));
                if (commandDefinition == null)
                {
                    validationResult.IsValid = false;
                    validationResult.AddMessage($"The command '{command.Name}' was not found among the configured commands ({String.Join(", ", config.Commands.Select(c => ((CommandConfig)c).Name))}).");
                }
                else
                {
                    commandDefinition.ParsedValue = command;

                    // Find each of the supplied switch 
                    foreach (CliArgument cmd in switches)
                    {
                        var cliSwitch = cmd.FindCommandSwitch(commandDefinition);
                        if (cliSwitch == null)
                        {
                            cliSwitch = cmd.FindCommandSwitch(config.Global);
                            if (cliSwitch == null)
                            {
                                var commandSwitchNames = $"the command's switches ({String.Join(", ", commandDefinition.Switches.Select(s => s.Name))}) or ";
                                this.ValidateBuiltInCommands(validationResult, cmd, config, commandSwitchNames);
                            }
                            else
                                cliSwitch.ParsedValue = cmd;
                        }
                        else
                            cliSwitch.ParsedValue = cmd;
                    }

                    if (commandDefinition.IsValueRequired && !commandDefinition.HasValueSet())
                    {
                        validationResult.IsValid = false;
                        validationResult.AddMessage($"No value was supplied for the command: '{command.Name}', and one was required ({commandDefinition.ValueHelpName}).");
                    }
                }
            }
            else if (parseResult.ResultType == ParseResultType.SwitchesOnly)
            {
                // Validate only against the built-in commands
                foreach (CliArgument cmd in parseResult.Arguments)
                    this.ValidateBuiltInCommands(validationResult, cmd, config);
            }
            else if (parseResult.ResultType == ParseResultType.Help)
            {
                config.Help.ParsedValue = parseResult.Arguments.First();

                // Validate only against the built-in commands
                var args = parseResult.Arguments.Skip(1);
                foreach (CliArgument cmd in args)
                    this.ValidateBuiltInCommands(validationResult, cmd, config);
            }


            var pr = (ParseResult)parseResult;

            if (!validationResult.IsValid)
                pr.ResultType = ParseResultType.Invalid;
            else
            {
                pr.Global = config.Global;
                pr.SelectedCommand = config.GetSelectedCommand();
            }

            pr.ValidationResult = validationResult;



            return validationResult;
        }

        private void ValidateBuiltInCommands(ValidationResult validationResult, CliArgument command, CliConfiguration config, string extraMessage = "")
        {
            var cliSwitch = command.FindCommandSwitch(config.Global);
            if (cliSwitch == null)
            {
                cliSwitch = command.FindCommandSwitch(config.Help);
                if (cliSwitch == null)
                {
                    validationResult.IsValid = false;
                    validationResult.AddMessage($"The switch '{command.Name}' could not be found among {extraMessage}the global command's switches ({String.Join(", ", config.Global.Switches.Select(s => s.Name))}).");
                }
                else
                    cliSwitch.ParsedValue = command;
            }
            else
                cliSwitch.ParsedValue = command;
        }
    }
}
