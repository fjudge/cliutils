﻿using System.Collections.Generic;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represetns a validation result.
    /// </summary>
    /// <seealso cref="FJSolutions.CommandLine.IValidationResult" />
    public class ValidationResult : IValidationResult
    {
        // Fields
        private List<string> _messages;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationResult"/> class.
        /// </summary>
        internal ValidationResult()
        {
            this._messages = new List<string>();
            this.IsValid = true;
        }

        public bool IsValid { get; set; }

        public string[] Messages
        {
            get { return this._messages.ToArray(); }
        }

        /// <summary>
        /// Adds a message tot he list of messages to return.
        /// </summary>
        /// <param name="message">The message.</param>
        internal void AddMessage(string message)
        {
            this._messages.Add(message);
        }
    }
}
