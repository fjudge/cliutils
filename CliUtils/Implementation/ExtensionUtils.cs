﻿using System;
using System.Linq;
using System.Text;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Contains helper methods used internally by the <see cref="CliUtils"/> library.
    /// </summary>
    internal static class ExtensionUtils
    {
        /// <summary>
        /// Trims leading dashes from the supplied argument.
        /// </summary>
        /// <param name="arg">The argument.</param>
        /// <returns>A <see cref="String"/> containing just the name of the argument</returns>
        public static string TrimArgument(this string arg)
        {
            return string.IsNullOrWhiteSpace(arg) ? string.Empty : arg.Trim(' ', '-');
        }

        /// <summary>
        /// Removes redundant white space from the supplied text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>A <see cref="String"/> containing the text without redundant white space.</returns>
        public static string RemoveRedundantWitespace(this string text)
        {
            var sb = new StringBuilder();
            var isWhitespace = false;
            var newLineCount = 0;

            for (int i = 0; i < text.Length; i++)
            {
                var c = text[i];

                if (c == '\r' || c == '\n')
                {
                    newLineCount += 1;

                    if (i + 1 < text.Length)
                    {
                        c = text[i + 1];
                        if (c == '\n' || c == '\r')
                            i += 1;
                    }

                    for (int j = sb.Length - 1; j >= 0; j--)
                    {
                        c = sb[j];
                        if (!char.IsWhiteSpace(c))
                        {
                            sb.Length = j + 1;
                            isWhitespace = false;
                            break;
                        }
                    }

                    continue;
                }

                if (char.IsWhiteSpace(c))
                {
                    if (!isWhitespace && newLineCount < 2)
                    {
                        sb.Append(' ');
                        isWhitespace = true;
                    }
                    continue;
                }

                if (newLineCount > 1)
                {
                    sb.AppendLine();
                }

                newLineCount = 0;
                isWhitespace = false;
                sb.Append(c);
            }

            return sb.ToString();
        }

        public static bool IsCommandName(this CommandConfig config, string name)
        {
            if (string.Equals(name, config.Name, StringComparison.CurrentCultureIgnoreCase))
                return true;

            if (string.Equals(name, config.ShortName, StringComparison.CurrentCultureIgnoreCase))
                return true;

            return false;
        }

        public static bool IsSwtichName(this SwitchConfig config, string name)
        {
            if (string.Equals(name, config.Name, StringComparison.CurrentCultureIgnoreCase))
                return true;

            if (string.Equals(name, config.ShortName, StringComparison.CurrentCultureIgnoreCase))
                return true;

            return false;
        }

        public static bool IsCommandArgument(this CliArgument arg, CommandConfig config)
        {
            if (string.Equals(arg.Name, config.Name, StringComparison.CurrentCultureIgnoreCase))
                return true;

            if (string.Equals(arg.Name, config.ShortName, StringComparison.CurrentCultureIgnoreCase))
                return true;

            return false;
        }

        //public static bool IsCommandSwitch(this CliArgument arg, SwitchConfig config)
        //{
        //    if (string.Equals(arg.Name, config.Name, StringComparison.CurrentCultureIgnoreCase))
        //        return true;

        //    if (string.Equals(arg.Name, config.ShortName, StringComparison.CurrentCultureIgnoreCase))
        //        return true;

        //    return false;
        //}

        //public static bool IsCommandSwitch(this CliArgument arg, CommandConfig config)
        //{
        //    foreach (var cliSwitch in config.Switches)
        //    {
        //        if (arg.IsCommandSwitch(cliSwitch))
        //            return true;
        //    }

        //    return false;
        //}

        public static bool IsNull(this ICliArgument argument)
        {
            return argument == null;
        }

        public static bool IsValueNullOrWhiteSpace(this ICliArgument argument)
        {
            if (argument.IsNull())
                return true;

            return string.IsNullOrWhiteSpace(argument.Value);
        }

        //public static string FindCommandSwitchValue(this CliArgument arg, CommandConfig config)
        //{
        //    var switchConfig = arg.FindCommandSwitch(config);
        //    if (switchConfig == null)
        //        return null;

        //    return switchConfig.ParsedValue.GetValue();
        //}

        public static string GetValue(this ICliArgument arg)
        {
            if (arg == null)
                return null;

            if (string.IsNullOrWhiteSpace(arg.Value))
                return null;

            return arg.Value.Trim();
        }

        public static SwitchConfig FindCommandSwitch(this CliArgument arg, CommandConfig config)
        {
            if (arg.IsNull())
                return null;

            return config.Switches
                .SingleOrDefault(s =>
                {
                    return string.Equals(arg.Name, s.Name, StringComparison.CurrentCultureIgnoreCase)
                        || string.Equals(arg.Name, s.ShortName, StringComparison.CurrentCultureIgnoreCase);
                });
        }
    }
}
