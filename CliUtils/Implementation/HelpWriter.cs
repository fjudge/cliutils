﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represents a utility class that can output help text to stream.
    /// </summary>
    public class HelpWriter : IHelpWriter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpWriter"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        internal HelpWriter(TextWriter stream, bool isConsoleOutput, HelpWriterOptions options)
        {
            this.Stream = stream;
            this.IsConsoleOutput = isConsoleOutput;
            this.Options = options;
        }

        public TextWriter Stream { get; private set; }

        public bool IsConsoleOutput { get; private set; }

        public HelpWriterOptions Options { get; private set; }

        public void WriteSuccess(string message, bool prefixWriteLine = false)
        {
            this.WriteLine(message, this.Options.SuccessColour, prefixWriteLine);
        }

        public void WriteError(string errorMessage, bool prefixWriteLine = false)
        {
            this.WriteLine(errorMessage, this.Options.ErrorColour, prefixWriteLine);
        }

        public void WriteInfo(string message, bool prefixWriteLine = false)
        {
            this.WriteLine(message, this.Options.DefaultColour, prefixWriteLine);
        }

        public void WriteSuccess(StringBuilder message, bool prefixWriteLine = false)
        {
            this.WriteLine(message.ToString(), this.Options.SuccessColour, prefixWriteLine);
            message.Clear();
        }

        public void WriteError(StringBuilder errorMessage, bool prefixWriteLine = false)
        {
            this.WriteLine(errorMessage.ToString(), this.Options.ErrorColour, prefixWriteLine);
            errorMessage.Clear();
        }

        public void WriteInfo(StringBuilder message, bool prefixWriteLine = false)
        {
            this.WriteLine(message.ToString(), this.Options.DefaultColour, prefixWriteLine);
            message.Clear();
        }

        public string[] SplitTextIntoLines(string text, bool autoIndentLines = true)
        {
            return this.SplitTextIntoLines(text, (autoIndentLines ? this.Options.CommandWidth : 0));
        }

        public string[] SplitTextIntoLines(string text, int indent, bool overrideDefaultFirstLineBehaviour = false)
        {
            // Break the output down into lines
            var sb = new StringBuilder();
            var width = this.Options.LineWidth - 1 - indent;
            //if (text.Length <= width)
            //{
            //    sb.Append(' ', indent).Append(text);
            //    return new string[] { sb.ToString() };
            //}

            var index = 0;
            var list = new List<string>();

            while (index < text.Length)
            {
                if (text.Length < index + width)
                    width = text.Length - index;

                var subString = text.Substring(index, width);
                index += width;

                // Check that the next character is a space or the end of the string,
                // otherwise work backwards to a space and re-SubString it.
                if (text.Length > index)
                {
                    var c = text[index];
                    var subWidth = subString.Length;
                    if (!char.IsWhiteSpace(c))
                    {
                        for (int i = width - 1; i >= 0; i--)
                        {
                            c = subString[i];
                            if (char.IsWhiteSpace(c))
                            {
                                index -= (width - i - 1);
                                subWidth = i;
                                break;
                            }
                        }

                        subString = subString.Substring(0, subWidth);
                    }
                    else
                        index += 1;
                }

                sb.Append(subString);

                list.Add(sb.ToString());
                sb.Clear();
            }

            // Add the indents
            int j = overrideDefaultFirstLineBehaviour ? 0 : 1;
            for (; j < list.Count; j++)
            {
                sb.Clear();
                for (int i = 0; i < indent; i++)
                    sb.Append(' ');

                sb.Append(list[j]);

                list[j] = sb.ToString();
            }


            return list.ToArray();
        }

        public void WriteAppInfo(IAppInfo appInfo)
        {
            var sb = new StringBuilder();
            var present = false;

            sb.Append(appInfo.Name);
            if (!string.IsNullOrWhiteSpace(appInfo.VersionName))
            {
                sb.Append(" (")
                    .Append(appInfo.VersionName);
                present = true;
            }
            else if (!string.IsNullOrWhiteSpace(appInfo.VersionNumber))
            {
                if (!present)
                    sb.Append(" (");

                sb.Append(appInfo.VersionNumber).Append(' ');
                present = true;
            }
            if (present)
                sb.Append(')');
            sb.AppendLine();

            present = false;
            if (!string.IsNullOrWhiteSpace(appInfo.Company))
            {
                sb.Append(appInfo.Company).Append(' ');
                present = true;
            }
            if (!string.IsNullOrWhiteSpace(appInfo.Copyright))
            {
                if (present)
                    sb.Append("(");

                sb.Append(appInfo.Copyright);

                if (present)
                    sb.Append(")");

                present = true;
            }
            if (present)
                sb.AppendLine();

            if (!string.IsNullOrWhiteSpace(appInfo.Description))
            {
                var lines = this.SplitTextIntoLines(appInfo.Description, false);
                foreach (var line in lines)
                {
                    sb.AppendLine(line);
                }
            }

            this.WriteInfo(sb);
        }

        /// <summary>
        /// Writes short-form help for the supplied <see cref="CliConfiguration"/>.
        /// </summary>
        /// <param name="config">The configuration.</param>
        internal void WriteShortHelp(CliConfiguration config)
        {
            var sb = new StringBuilder();

            // Commands
            if (config.Commands.Length > 0)
            {

                this.WriteInfo("Commands:", true);
                foreach (CommandConfig cmd in config.Commands)
                {
                    // Write out the command name and the Manual command text
                    this.WriteShortHelp(cmd, sb);

                    foreach (var sw in cmd.Switches)
                    {
                        // Write out the indented switch name and the Manual help text
                        this.WriteShortHelp(sw, sb);
                    }
                }
            }

            // Global
            if (config.Global.Switches.Length > 0)
            {
                this.WriteInfo("Global Switches:", true);
                foreach (var sw in config.Global.Switches)
                {
                    this.WriteShortHelp(sw, sb);
                }
            }

            // Help
            this.WriteInfo("Help Command:", true);
            this.WriteShortHelp(config.Help, sb);
            foreach (var sw in config.Help.Switches)
            {
                this.WriteShortHelp(sw, sb);
            }

            // Example Usage
            this.WriteUsageHelp(config.HelpInfo.ExampleUsage, sb);
        }

        private void WriteShortHelp(CommandConfig command, StringBuilder sb)
        {
            sb.Append(command.Name);
            if (!string.IsNullOrWhiteSpace(command.ShortName))
                sb.Append(", ")
                    .Append(command.ShortName);

            if (!string.IsNullOrWhiteSpace(command.ValueHelpName))
                sb.Append(" <")
                    .Append(command.ValueHelpName)
                    .Append('>');

            var length = sb.Length;

            for (int i = length; i < this.Options.CommandWidth; i++)
            {
                sb.Append(' ');
            }

            if (command.HelpInfo.LongDescription != null)
            {
                foreach (var paragraph in command.HelpInfo.LongDescription)
                {
                    var lines = this.SplitTextIntoLines(paragraph, false);
                    foreach (var line in lines)
                        sb.AppendLine(line);
                }
            }

            sb.Length -= Environment.NewLine.Length;

            this.WriteInfo(sb);
        }

        private void WriteShortHelp(SwitchConfig @switch, StringBuilder sb)
        {
            for (int i = 0; i < this.Options.CommandSwitchIndent; i++)
                sb.Append(' ');

            sb.Append("--")
                .Append(@switch.Name);
            if (!string.IsNullOrWhiteSpace(@switch.ShortName))
                sb.Append(", -")
                  .Append(@switch.ShortName);
            if (!string.IsNullOrWhiteSpace(@switch.ValueHelpName))
                sb.Append(" <")
                    .Append(@switch.ValueHelpName)
                    .Append('>');

            var length = sb.Length;

            for (int i = length; i < this.Options.CommandWidth; i++)
            {
                sb.Append(' ');
            }

            if (!string.IsNullOrWhiteSpace(@switch.HelpInfo.Description))
            {
                var lines = this.SplitTextIntoLines(@switch.HelpInfo.Description);
                for (int i = 0; i < lines.Length; i++)
                {
                    sb.AppendLine(lines[i]);
                }
            }

            sb.Length -= Environment.NewLine.Length;

            this.WriteInfo(sb);
        }

        /// <summary>
        /// Writes manual-style help.
        /// </summary>
        /// <param name="config">The configuration.</param>
        internal void WriteManHelp(CliConfiguration config)
        {
            var sb = new StringBuilder();
            int ordinal = 1;
            var commands = new List<CommandConfig>(config.Commands.Cast<CommandConfig>());
            commands.Add(config.Global);
            commands.Add(config.Help);

            // Application description & usage
            this.WriteManHelpInfo(config.HelpInfo, sb);
            sb.AppendLine();

            // Table of Contents
            sb.AppendLine("Commands:");
            sb.Append('=', sb.Length - (Environment.NewLine.Length * 2))
                .AppendLine();
            foreach (var cmd in commands)
            {
                sb.Append(ordinal)
                    .Append(". ")
                    .AppendLine(cmd.Name);
                ordinal += 1;
            }
            sb.AppendLine();
            this.WriteInfo(sb);

            // Help details
            ordinal = 1;
            foreach (var cmd in commands)
            {
                this.WriteManHelp(cmd, sb, ordinal);
                ordinal += 1;
            }
        }

        internal void WriteManHelp(CommandConfig command, StringBuilder sb, int? ordinal = null)
        {
            // Write out the command name and the short command text
            if (ordinal.HasValue)
                sb.Append(ordinal).Append(". ");
            sb.AppendLine(command.Name)
                .Append('=', sb.Length - Environment.NewLine.Length)
                .AppendLine()
                .AppendLine()
                .Append("Command  Name: ")
                .Append(command.Name);

            if (!string.IsNullOrWhiteSpace(command.ValueHelpName))
                sb.Append(" <")
                    .Append(command.ValueHelpName)
                    .Append('>');

            sb.AppendLine();

            if (!string.IsNullOrWhiteSpace(command.ShortName))
                sb.Append("Short Name: ")
                    .AppendLine(command.ShortName)
                    .AppendLine();

            this.WriteManHelpInfo(command.HelpInfo, sb);

            if (command.HasSwitches)
            {
                var title = "Command Switches";
                title = title.PadLeft(this.Options.CommandSwitchIndent + title.Length, ' ');
                sb.AppendLine()
                    .AppendLine(title)
                    .Append(' ', this.Options.CommandSwitchIndent)
                    .Append('-', title.Length - this.Options.CommandSwitchIndent)
                    .AppendLine()
                    .AppendLine();

                // Write out the indented switch name and the short help text
                foreach (var sw in command.Switches)
                {
                    sb.Append(' ', this.Options.CommandSwitchIndent)
                        .Append("--")
                        .Append(sw.Name);

                    if (!string.IsNullOrWhiteSpace(sw.ShortName))
                        sb.Append(", -")
                            .Append(sw.ShortName);

                    if (!string.IsNullOrWhiteSpace(sw.ValueHelpName))
                        sb.Append(" <")
                            .Append(sw.ValueHelpName)
                            .Append('>');

                    sb.Append(" ");

                    this.WriteInfo(sb);

                    this.WriteManHelpInfo(sw.HelpInfo, sb, this.Options.CommandSwitchIndent);
                }
            }

            if (!string.IsNullOrWhiteSpace(command.HelpInfo.ExampleUsage))
            {
                sb.AppendLine()
                    .AppendLine("Example Usage:")
                    .Append(' ', this.Options.CommandSwitchIndent * 2)
                    .AppendLine()
                    .AppendLine(command.HelpInfo.ExampleUsage)
                    .AppendLine();
            }

            this.WriteInfo(sb);
        }

        private void WriteManHelpInfo(HelpInfo helpInfo, StringBuilder sb, int indent = 0)
        {
            if (helpInfo == null)
                return;

            if (!string.IsNullOrWhiteSpace(helpInfo.Description))
            {
                var lines = this.SplitTextIntoLines(helpInfo.Description, false);
                foreach (var line in lines)
                    sb.Append(' ', indent)
                        .AppendLine(line);

                this.WriteInfo(sb);
            }

            if (helpInfo.LongDescription != null)
            {
                sb.AppendLine();
                foreach (var paragraph in helpInfo.LongDescription)
                {
                    var lines = this.SplitTextIntoLines(paragraph, false);
                    foreach (var line in lines)
                        sb.Append(' ', indent)
                            .AppendLine(line);

                    this.WriteInfo(sb);
                }
            }

            if (!string.IsNullOrWhiteSpace(helpInfo.ExampleUsage))
            {
                sb.Append(' ', indent)
                    .AppendLine("Example / Usage:");
                var lines = this.SplitTextIntoLines(helpInfo.ExampleUsage, indent + this.Options.CommandSwitchIndent, true);
                foreach (var line in lines)
                    sb.AppendLine(line);

                this.WriteInfo(sb);
            }
        }

        ///// <summary>
        ///// Writes the long (Manual) help format for the supplied <see cref="CommandConfig"/>.
        ///// </summary>
        ///// <param name="command">The command.</param>
        ///// <param name="sb">The <see cref="StringBuilder"/> to use to build the help text.</param>
        //internal void WriteLongHelp(CommandConfig command, StringBuilder sb)
        //{
        //}

        ///// <summary>
        ///// Writes the long (Manual) help format for the supplied <see cref="SwitchConfig"/>.
        ///// </summary>
        ///// <param name="switch">The switch.</param>
        ///// <param name="sb">The <see cref="StringBuilder"/> to use to build the help text.</param>
        //internal void WriteLongtHelp(SwitchConfig @switch, StringBuilder sb)
        //{
        //}

        /// <summary>
        /// Writes the help usage / example text if present.
        /// </summary>
        /// <param name="helpUsage">The help usage.</param>
        /// <param name="sb">A <see cref="StringBuilder"/>.</param>
        public void WriteUsageHelp(string helpUsage, StringBuilder sb)
        {
            if (string.IsNullOrWhiteSpace(helpUsage))
                return;

            // Format the usage text and then write with a prepended line
            sb.Append(' ', this.Options.CommandSwitchIndent)
                .AppendLine("Example usage:")
                .Append(' ', this.Options.CommandSwitchIndent * 2)
                .Append(helpUsage);

            this.WriteLine();
            this.WriteInfo(sb);
        }

        /// <summary>
        /// Writes an empty line to the output stream.
        /// </summary>
        public void WriteLine()
        {
            this.Stream.WriteLine();
        }

        private void WriteLine(string message, ConsoleColor colour, bool prefixWriteLine)
        {
            if (prefixWriteLine)
                this.Stream.WriteLine();

            if (this.IsConsoleOutput)
                Console.ForegroundColor = colour;

            this.Stream.WriteLine(message);

            if (this.IsConsoleOutput)
                Console.ForegroundColor = this.Options.DefaultColour;
        }
    }
}
