﻿namespace FJSolutions.CommandLine.Implementation
{
    public class AppInfo : IAppInfo
    {
        public string Name { get; set; }

        public string Company { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public string VersionName { get; set; }

        public string VersionNumber { get; set; }
    }
}
