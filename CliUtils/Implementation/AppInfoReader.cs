﻿using System.Reflection;

namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Utility class for readong AssemblyInfo from an assembly
    /// </summary>
    internal static class AppInfoReader
    {
        /// <summary>
        /// Gets the assembly display information.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>An array of strings</returns>
        internal static AppInfo GetInfo(Assembly assembly)
        {
            var vi = new AppInfo
            {
                Name = AppInfoReader.GetAssemblyTitle(assembly) ?? string.Empty,
                Company = AppInfoReader.GetCompanyName(assembly) ?? string.Empty,
                Description = AppInfoReader.GetDescription(assembly) ?? string.Empty,
                VersionName = AppInfoReader.GetVersionName(assembly) ?? string.Empty,
                VersionNumber = AppInfoReader.GetVersionNumber(assembly) ?? string.Empty,
                Copyright = AppInfoReader.GetCopyright(assembly) ?? string.Empty
            };

            return vi;
        }

        private static string GetCompanyName(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyCompanyAttribute>();
            if (attribute != null)
                return attribute.Company;

            return null;
        }

        private static string GetAssemblyTitle(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyTitleAttribute>();
            if (attribute != null && !string.IsNullOrWhiteSpace(attribute.Title))
                return attribute.Title;

            return assembly.GetName().Name;
        }

        private static string GetCopyright(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyCopyrightAttribute>();
            if (attribute != null)
                return attribute.Copyright;

            return null;
        }

        private static string GetVersionNumber(Assembly assembly)
        {
            var fileVersion = assembly.GetCustomAttribute<AssemblyFileVersionAttribute>();
            if (fileVersion != null)
                return fileVersion.Version;

            //var assemblyVersionAttribute = assembly.GetCustomAttribute<AssemblyVersionAttribute>();
            //if (assemblyVersionAttribute != null)
            //    return assemblyVersionAttribute.Version;

            return null;
        }

        private static string GetVersionName(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (attribute != null)
                return attribute.InformationalVersion;

            return null;
        }

        private static string GetDescription(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>();
            if (attribute != null)
                return attribute.Description;

            return null;
        }
    }
}