﻿namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represents the result of parsing command line arguments
    /// </summary>
    /// <seealso cref="FJSolutions.CommandLine.IParseResult" />
    public class ParseResult : IParseResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseResult"/> class.
        /// </summary>
        internal ParseResult()
        {
        }

        public ICliArgument[] Arguments { get; set; }

        public ParseResultType ResultType { get; set; }

        public bool HasBeenValidated
        {
            get { return this.ValidationResult != null; }
        }

        public IParsedCommand Global { get; set; }

        public IParsedCommand SelectedCommand { get; set; }

        public IValidationResult ValidationResult { get; set; }
    }
}
