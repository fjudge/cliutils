﻿using System;
using System.IO;
using System.Xml;

namespace FJSolutions.CommandLine.Implementation
{
    internal class XmlConfigurationLoader
    {
        // Fields
        private XmlDocument _xDoc;
        private XmlNamespaceManager _nsManager;

        internal XmlConfigurationLoader()
        {
        }

        public XmlConfigurationLoader LoadConfig(TextReader stream)
        {
            using (stream)
            {
                var nt = new NameTable();
                this._nsManager = new XmlNamespaceManager(nt);
                this._nsManager.AddNamespace("FBJ", "http://fjsolutions.co.za/CliUtilsConfig.xsd");

                this._xDoc = new XmlDocument(nt);
                this._xDoc.Load(stream);
            }

            return this;
        }

        public XmlConfigurationLoader BuildConfig()
        {
            this.Config = new CliConfiguration();

            // App Info
            var appElement = this.GetElement(this._xDoc.DocumentElement, "appInfo");
            if (appElement != null)
                this.ProcessAppInfoElement(appElement);

            // Commands
            foreach (XmlElement commandXml in this.GetNodes(this._xDoc.DocumentElement, "command"))
            {
                this.ProcessCommandElement(commandXml);
            }

            // Help info
            var helpElement = this.GetElement(this._xDoc.DocumentElement, "helpInfo");
            if (helpElement != null)
                this.ProcessHelpInfoElement(helpElement, this.Config.HelpInfo);

            return this;
        }

        private void ProcessHelpInfoElement(XmlElement helpElement, HelpInfo helpInfo)
        {
            var element = this.GetElement(helpElement, "description");
            if (element != null)
            {
                var text = this.GetTextValue(element);
                if (!string.IsNullOrWhiteSpace(text))
                    helpInfo.SetHelpDescription(text);
            }

            element = this.GetElement(helpElement, "longDescription");
            if (element != null)
            {
                var text = this.GetTextValue(element);
                if (!string.IsNullOrWhiteSpace(text))
                    this.Config.SetHelpLongDescription(text);
            }

            element = this.GetElement(helpElement, "exampleUsage");
            if (element != null)
            {
                var text = this.GetTextValue(element);
                if (!string.IsNullOrWhiteSpace(text))
                    this.Config.SetHelpExampleUsage(text);
            }
        }

        private void ProcessAppInfoElement(XmlElement appElement)
        {
            this.Config.AppInfo = new AppInfo();

            var element = this.GetElement(appElement, "name");
            if (element != null)
                this.Config.AppInfo.Name = this.GetTextValue(element);

            element = this.GetElement(appElement, "company");
            if (element != null)
                this.Config.AppInfo.Company = this.GetTextValue(element);

            element = this.GetElement(appElement, "copyright");
            if (element != null)
                this.Config.AppInfo.Copyright = this.GetTextValue(element);

            element = this.GetElement(appElement, "description");
            if (element != null)
                this.Config.AppInfo.Description = this.GetTextValue(element);

            element = this.GetElement(appElement, "versionNumber");
            if (element != null)
                this.Config.AppInfo.VersionNumber = this.GetTextValue(element);

            element = this.GetElement(appElement, "versionName");
            if (element != null)
                this.Config.AppInfo.VersionName = this.GetTextValue(element);
        }

        public CliConfiguration Config { get; private set; }

        private void ProcessCommandElement(XmlElement commandElement)
        {
            string name = null;
            string shortName = null;

            var attribute = commandElement.GetAttribute("name");
            if (attribute != null)
                name = attribute.Trim();

            attribute = commandElement.GetAttribute("shortName");
            if (attribute != null)
                shortName = attribute.Trim();

            var config = (CommandConfig)this.Config.AddCommand(name, shortName);

            var element = this.GetElement(commandElement, "helpInfo");
            if (element != null)
                this.ProcessHelpInfoElement(element, config.HelpInfo);

            // Value help name & is required
            element = this.GetElement(commandElement, "valueHelpName");
            if (element != null)
            {
                var isRequired = true;
                var valueHelpName = this.GetTextValue(element);
                attribute = element.GetAttribute("required");
                if (attribute != null)
                    isRequired = this.GetBooleanValue(attribute);
                attribute = element.GetAttribute("valueType");
                CommandValueType valueType;
                Enum.TryParse<CommandValueType>(attribute, out valueType);

                config.SetValueType(valueType, valueHelpName);
                config.SetIsValueRequired(isRequired);
            }

            // Process any switches 
            element = this.GetElement(commandElement, "switches");
            if (element != null)
            {
                foreach (XmlElement switchElement in this.GetNodes(element, "switch"))
                {
                    this.ProcessSwitchElement(config, switchElement);
                }
            }
        }

        private bool GetBooleanValue(string attribute)
        {
            if (string.IsNullOrWhiteSpace(attribute))
                return false;

            if (attribute.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                return true;
            else if (attribute.Equals("on", StringComparison.CurrentCultureIgnoreCase))
                return true;
            else if (attribute.Equals("yes", StringComparison.CurrentCultureIgnoreCase))
                return true;
            else if (attribute.Equals("1", StringComparison.CurrentCultureIgnoreCase))
                return true;

            return false;
        }

        private void ProcessSwitchElement(CommandConfig command, XmlElement switchElement)
        {
            string name = null;
            string shortName = null;

            var attribute = switchElement.GetAttribute("name");
            if (attribute != null)
                name = attribute.Trim();

            attribute = switchElement.GetAttribute("shortName");
            if (attribute != null)
                shortName = attribute.Trim();

            var sw = (SwitchConfig)command.AddSwitch(name, shortName);

            // Value help name & is required
            var element = this.GetElement(switchElement, "valueHelpName");
            if (element != null)
            {
                var isRequired = true;
                var valueHelpName = this.GetTextValue(element);
                attribute = element.GetAttribute("required");
                if (attribute != null)
                    isRequired = this.GetBooleanValue(attribute);

                sw.SetValueHelpName(valueHelpName, isRequired);
            }

            var helpElement = this.GetElement(switchElement, "helpInfo");
            if (helpElement != null)
                this.ProcessHelpInfoElement(helpElement, sw.HelpInfo);
        }

        private string GetTextValue(XmlNode element)
        {
            if (element == null)
                return null;

            string text = null;

            if (element.HasChildNodes && element.LastChild != null && element.LastChild.NodeType == XmlNodeType.CDATA)
                text = ((XmlCDataSection)element.LastChild).InnerText;
            else
                text = element.InnerText;

            if (string.IsNullOrWhiteSpace(text))
                return null;

            return text.Trim();
        }

        private XmlElement GetElement(XmlElement parent, string elementName)
        {
            return (XmlElement)parent.SelectSingleNode("FBJ:" + elementName, this._nsManager);
        }

        private XmlNodeList GetNodes(XmlElement parent, string xPath)
        {
            return parent.SelectNodes("FBJ:" + xPath, this._nsManager);
        }
    }
}
