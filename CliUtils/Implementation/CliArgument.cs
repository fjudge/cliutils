﻿namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Represent information about an argument that has been parsed.
    /// </summary>
    /// <seealso cref="FJSolutions.CommandLine.ICliArgument" />
    public class CliArgument : ICliArgument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CliArgument"/> class.
        /// </summary>
        internal CliArgument()
        {
        }

        public CliArgumentType ArgumentType { get; set; }

        public bool HasValue
        {
            get { return !string.IsNullOrWhiteSpace(this.Value); }
        }

        public string Name { get; set; }

        public int Position { get; set; }

        public string Raw { get; set; }

        public string Value { get; set; }
    }
}
