﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FJSolutions.CommandLine.Implementation
{
    public class CliConfiguration : ICliConfiguration
    {
        // Fields
        private List<CommandConfig> _commands;

        /// <summary>
        /// Initializes a new instance of the <see cref="CliConfiguration"/> class.
        /// </summary>
        internal CliConfiguration()
        {
            this.HelpInfo = new HelpInfo();
            this._commands = new List<CommandConfig>();
            this.Global = new CommandConfig("Global", this);
            this.Global.AddSwitch("NoLogo", "nl")
                .SetHelpDescription("This switch indicates whether the automatic outputting of application information should be suppressed.");
            this.Help = new CommandConfig("Help", this);
            this.Help.SetShortName("?")
                .SetHelpDescription("Shows this help text.")
                .AddSwitch("File", "f")
                .SetHelpDescription("Specifies a file name to output the help text to.")
                .Command
                .AddSwitch("Manual", "man")
                .SetHelpDescription("Output the long version of the help text in manual format.");
        }

        public ICommandConfig[] Commands
        {
            get { return this._commands.ToArray(); }
        }

        public CommandConfig Help { get; set; }

        public CommandConfig Global { get; set; }

        ICommandConfig ICliConfiguration.Global
        {
            get { return this.Global; }
        }

        public IAppInfo AppInfo { get; set; }

        public HelpInfo HelpInfo { get; set; }

        public ICommandConfig this[string name]
        {
            get { return this._commands.SingleOrDefault(c => c.IsCommandName(name)); }
        }

        public ICommandConfig AddCommand(string commandName)
        {
            return this.AddCommand(commandName, null);
        }

        public ICommandConfig AddCommand(string commandName, string shortName)
        {
            if (string.IsNullOrWhiteSpace(commandName))
                throw new ArgumentException("commandName");

            var config = new CommandConfig(commandName, this);
            config.SetShortName(shortName);

            this._commands.Add(config);

            return config;
        }

        public ICliConfiguration SetAppInfo(Type type)
        {
            if (type == null)
                throw new ArgumentException("type");

            this.AppInfo = AppInfoReader.GetInfo(type.Assembly);

            return this;
        }

        public ICliConfiguration SetAppInfo()
        {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly == null)
                assembly = Assembly.GetCallingAssembly();

            this.AppInfo = AppInfoReader.GetInfo(assembly);

            return this;
        }

        public ICliConfiguration SetHelpDescription(string helpDescription)
        {
            this.HelpInfo.SetHelpDescription(helpDescription);
            return this;
        }

        public ICliConfiguration SetHelpExampleUsage(string exampleUsage)
        {
            this.HelpInfo.SetHelpExampleUsage(exampleUsage);
            return this;
        }

        public ICliConfiguration SetHelpLongDescription(string longHelp)
        {
            this.HelpInfo.SetHelpLongDescription(longHelp);
            return this;
        }

        public IParsedCommand GetSelectedCommand()
        {
            if (((CommandConfig)this.Help).IsSelected)
                return (IParsedCommand)this.Help;

            if (((CommandConfig)this.Global).IsSelected)
                return (IParsedCommand)this.Global;

            return this._commands.SingleOrDefault(c => c.IsSelected);
        }
    }
}
