﻿namespace FJSolutions.CommandLine.Implementation
{
    /// <summary>
    /// Contains configuration information for registering an <see cref="ICliArgument"/>
    /// </summary>
    public class SwitchConfig : IParsedSwitch, ISwitchConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchConfig" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parent">The parent.</param>
        internal SwitchConfig(string name, CommandConfig parent)
        {
            this.HelpInfo = new HelpInfo();
            this.Name = name.TrimArgument();
            this.Command = parent;
        }
        public ICommandConfig Command { get; private set; }

        public HelpInfo HelpInfo { get; set; }

        public string Name { get; private set; }

        public string ShortName { get; private set; }

        public bool IsValueRequired { get; set; }

        /// <summary>
        /// Gets the name of the value to be displayed in the help text.
        /// </summary>
        public string ValueHelpName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="ICliArgument"/> has a value.
        /// </summary>
        public bool HasValue { get; private set; }

        /// <summary>
        /// Gets the <see cref="ICliArgument"/> for this switch if it was parsed from the command line.
        /// </summary>
        public ICliArgument ParsedValue { get; internal set; }

        /// <summary>
        /// Gets the parsed value of the switch, if one was set.
        /// </summary>
        public string Value
        {
            get { return this.ParsedValue == null ? string.Empty : this.ParsedValue.Value; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="SwitchConfig"/> has been set in in a parse.
        /// </summary>
        public bool IsSet
        {
            get { return this.ParsedValue != null; }
        }

        /// <summary>
        /// Sets a value indicating whether this <see cref="ISwitchConfig"/> should have a value.
        /// </summary>
        /// <param name="hasValue">if set to <c>true</c> then this <see cref="ISwitchConfig"/> should have a value.</param>
        /// <returns>This <see cref="ISwitchConfig"/> for chaining purposes</returns>
        public ISwitchConfig SetHasValue(bool hasValue = true)
        {
            this.HasValue = hasValue;

            return this;
        }

        public ISwitchConfig SetShortName(string shortName)
        {
            this.ShortName = shortName.TrimArgument();

            return this;
        }

        public ISwitchConfig SetHelpDescription(string helpText)
        {
            this.HelpInfo.SetHelpDescription(helpText);
            return this;
        }

        public ISwitchConfig SetHelpLongDescription(string helpText)
        {
            this.HelpInfo.SetHelpLongDescription(helpText);
            return this;
        }

        public ISwitchConfig SetHelpExampleUsage(string helpText)
        {
            this.HelpInfo.SetHelpExampleUsage(helpText);
            return this;
        }

        public ISwitchConfig SetValueHelpName(string valueNameHelp, bool isValueRequired = true)
        {
            if (string.IsNullOrWhiteSpace(valueNameHelp))
                this.ValueHelpName = null;
            else
                this.ValueHelpName = valueNameHelp.Trim();

            this.IsValueRequired = isValueRequired;

            return this;
        }

        public ISwitchConfig SetIsValueRequired(bool isValueRequired = true)
        {
            this.IsValueRequired = isValueRequired;
            return this;
        }
    }
}
