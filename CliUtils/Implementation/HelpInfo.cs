﻿using System;

namespace FJSolutions.CommandLine.Implementation
{
    public class HelpInfo : IHelpInfo
    {
        public string Description { get; private set; }

        public string ExampleUsage { get; private set; }

        public string[] LongDescription { get; private set; }

        public void SetHelpDescription(string helpDescription)
        {
            this.Description = helpDescription == null ? string.Empty : helpDescription.RemoveRedundantWitespace().Trim();
        }

        public void SetHelpExampleUsage(string exampleUsage)
        {
            this.ExampleUsage = exampleUsage == null ? string.Empty : exampleUsage.RemoveRedundantWitespace().Trim();
        }

        public void SetHelpLongDescription(string longHelp)
        {
            if (string.IsNullOrWhiteSpace(longHelp))
                this.LongDescription = null;
            else
            {
                var text = longHelp.RemoveRedundantWitespace().Trim();
                this.LongDescription = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            }
        }
    }
}
