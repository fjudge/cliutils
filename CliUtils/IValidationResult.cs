﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for a validation result
    /// </summary>
    public interface IValidationResult
    {
        /// <summary>
        /// Indicates whether validation was successful or not.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Gets an array of messages associated with the validation.
        /// </summary>
        string[] Messages { get; }
    }
}
