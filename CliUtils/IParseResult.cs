﻿namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Defines the interface for a parse result.
    /// </summary>
    public interface IParseResult
    {
        /// <summary>
        /// Gets the type of the result.
        /// </summary>
        ParseResultType ResultType { get; }

        /// <summary>
        /// Gets the array of arguments parsed from the command line.
        /// </summary>
        ICliArgument[] Arguments { get; }

        /// <summary>
        /// Gets a value indicating whether this instance has been validated.
        /// </summary>
        bool HasBeenValidated { get; }

        /// <summary>
        /// Gets the validation result of the <see cref="IParseResult"/> has been validated.
        /// </summary>
        IValidationResult ValidationResult { get; }

        /// <summary>
        /// Gets the parsed global command object.
        /// </summary>
        IParsedCommand Global { get; }

        /// <summary>
        /// Gets the selected command parsed by the parser.
        /// </summary>
        IParsedCommand SelectedCommand { get; }
    }
}
