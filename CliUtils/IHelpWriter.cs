﻿using System.Text;

namespace FJSolutions.CommandLine
{
    /// <summary>
    /// Refines the interface for a generic help text writer.
    /// </summary>
    public interface IHelpWriter
    {
        /// <summary>
        /// Writes a success message to the output.
        /// </summary>
        /// <param name="infoMessage">The message.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteSuccess(string infoMessage, bool prefixWriteLine = false);

        /// <summary>
        /// Writes a success message to the output.
        /// </summary>
        /// <param name="infoMessageBuilder">The <see cref="StringBuilder"/> message, which is reset after the message has been written.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteSuccess(StringBuilder infoMessageBuilder, bool prefixWriteLine = false);

        /// <summary>
        /// Writes an information message to the output.
        /// </summary>
        /// <param name="infoMessageBuilder">The <see cref="StringBuilder"/> message, which is reset after the message has been written.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteInfo(StringBuilder infoMessageBuilder, bool prefixWriteLine = false);

        /// <summary>
        /// Writes an information message to the output.
        /// </summary>
        /// <param name="infoMessage">The message.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteInfo(string infoMessage, bool prefixWriteLine = false);

        /// <summary>
        /// Writes an error message to the output.
        /// </summary>
        /// <param name="errorMessage">The message.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteError(string errorMessage, bool prefixWriteLine = false);

        /// <summary>
        /// Writes an error message to the output.
        /// </summary>
        /// <param name="errorMessageBuilder">The <see cref="StringBuilder"/> message, which is reset after the message has been written.</param>
        /// <param name="prefixWriteLine">if set to <c>true</c> a new line is prefixed to the output.</param>
        void WriteError(StringBuilder errorMessageBuilder, bool prefixWriteLine = false);

        /// <summary>
        /// Writes a new line to the output.
        /// </summary>
        void WriteLine();

    }
}
