﻿using NUnit.Framework;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace FJSolutions.CommandLine.Tests
{
    [TestFixture]
    public class HelpWriterTests
    {
        private string GetAssemblyDir()
        {
            var path = Path.GetDirectoryName(Assembly.GetAssembly(typeof(CliUtils)).Location);

            return path;
        }

        [Test]
        public void TestHelpWriterErrorMessage()
        {
            var config = CliUtils.CreateConfiguration();
            var writer = CliUtils.WriteAppTitle(config);

            Assert.That(writer, Is.Not.Null);

            var sb = new StringBuilder();
            sb.AppendLine("This is an error");
            writer.WriteError(sb);
        }

        [Test]
        public void TestHelpWriterSuccessMessage()
        {
            var config = CliUtils.CreateConfiguration();
            var writer = CliUtils.WriteAppTitle(config);

            Assert.That(writer, Is.Not.Null);

            var sb = new StringBuilder();
            sb.AppendLine("This is a success");
            writer.WriteSuccess(sb);
        }

        [Test]
        public void TestHelpWriterErrorMessageText()
        {
            var config = CliUtils.CreateConfiguration();
            var writer = CliUtils.WriteAppTitle(config);

            Assert.That(writer, Is.Not.Null);

            writer.WriteError("This is an error");
        }

        [Test]
        public void TestHelpWriterSuccessMessageText()
        {
            var config = CliUtils.CreateConfiguration();
            var writer = CliUtils.WriteAppTitle(config);

            Assert.That(writer, Is.Not.Null);

            writer.WriteSuccess("This is a success");
        }

        [Test]
        public void TestWritingHelpWithApplicationInfo()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils));

            CliUtils.WriteAppTitle(config);
        }

        [Test]
        public void TestWritingAppTitleWithNullConfig()
        {
            Assert.Throws<ArgumentException>(() => CliUtils.WriteAppTitle(null));
        }

        [Test]
        public void TestWritingHelpWithBlankApplicationInfo()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo();

            CliUtils.WriteAppTitle(config);
        }

        [Test]
        public void TestWritingHelpWithNullConfig()
        {
            Assert.Throws<ArgumentException>(() => CliUtils.WriteHelp(null));
        }

        [Test]
        public void TestWritingHelpWithParsedHelpSwitches()
        {
            var config = CliUtils.CreateConfiguration();
            var parser = CliUtils.CreateParser(config);
            parser.Parse("--NoLogo");
            var options = new HelpWriterOptions { LineWidth = 120 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-NoLogo.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingHelpWithParsedHelpOnComandNameSwitches()
        {
            var config = CliUtils.CreateConfiguration();
            var parser = CliUtils.CreateParser(config);
            parser.Parse($"Help --File \"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-File.txt\" ");
            var options = new HelpWriterOptions { LineWidth = 120 };
            CliUtils.WriteHelp(config, options: options);
        }

        [Test]
        public void TestWritingHelpWithParsedManualHelpSwitches()
        {
            var config = CliUtils.CreateConfiguration();
            var parser = CliUtils.CreateParser(config);
            parser.Parse("Help -man");
            var options = new HelpWriterOptions { LineWidth = 120 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-Manual.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingHelpWithParsedCommandName()
        {
            var config = CliUtils.CreateConfiguration();
            var parser = CliUtils.CreateParser(config);
            parser.Parse("Help Init");
            var options = new HelpWriterOptions { LineWidth = 120 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-CommandName.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingHelpWithGlobalCommand()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils))
                .Global
                .AddSwitch("Verbose", "v")
                .SetHelpDescription("Indicates whether the application should emit verbose output.")
                .Command
                .Config
                .AddCommand("Install", "i")
                .SetHelpDescription("Installs the supplied packages into the application, taking great care to do it properly!")
                .AddSwitch("Global", "g")
                .SetHelpDescription("Indicates that this should be installed globally so that everyone can access it and do exact what they like with it without making sure it works.")
                .Command
                .Config
                .SetHelpExampleUsage("app.exe Install <package> --Global --Verbose");
            var options = new HelpWriterOptions { LineWidth = 100 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-ShortHelp.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingManHelpWithGlobalCommand()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils))
                .Global
                .AddSwitch("Verbose", "v")
                .Command
                .Config
                .AddCommand("Install", "i")
                .SetHelpDescription("Installs the supplied packages into the application, taking great care to do it properly!")
                .AddSwitch("Global", "g")
                .SetHelpDescription("Indicates that this should be installed globally so that everyone can access it and do exactly what they like with it without making sure it works!")
                .Command
                .Config
                .SetHelpExampleUsage("app.exe Install <package> --Global --Verbose");
            CliUtils.CreateParser(config).Parse("Help --Manual");
            var options = new HelpWriterOptions { LineWidth = 100 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-ManHelp.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingManHelpWithNamedCommand()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils))
                .Global
                .AddSwitch("Verbose", "v")
                .SetHelpDescription("Indicates whether the application should emit verbose output.")
                .Command
                .Config
                .AddCommand("Install", "i")
                .SetHelpDescription("Installs the supplied packages into the application, taking great care to do it properly!")
                .SetHelpLongDescription("This is the long help text, and is some additional information about the command.")
                .SetHelpExampleUsage("Install <package> --Global")
                .AddSwitch("Global", "g")
                .SetHelpDescription("Indicates that this should be installed globally so that everyone can access it and do exactly what they like with it without making sure it works!")
                .SetHelpLongDescription("Some additional help text for the switch that makes things even clearer than before!")
                .Command
                .Config
                .SetHelpExampleUsage("app.exe Install <package> --Global --Verbose");
            CliUtils.CreateParser(config).Parse("Help Install");
            var options = new HelpWriterOptions { LineWidth = 100 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-CommandHelp.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingManHelpWithNamedGlobalCommand()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils))
                .Global
                .AddSwitch("Verbose", "v")
                .Command
                .Config
                .AddCommand("Install", "i")
                .SetHelpDescription("Installs the supplied packages into the application, taking great care to do it properly!")
                .AddSwitch("Global", "g")
                .SetHelpDescription("Indicates that this should be installed globally so that everyone can access it and do exactly what they like with it without making sure it works!")
                .Command
                .Config
                .SetHelpExampleUsage("app.exe Install <package> --Global --Verbose");
            CliUtils.CreateParser(config).Parse("Help Global");
            var options = new HelpWriterOptions { LineWidth = 100 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-GlobalCommandHelp.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

        [Test]
        public void TestWritingManHelpWithNamedHelpCommand()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils))
                .Global
                .AddSwitch("Verbose", "v")
                .Command
                .Config
                .AddCommand("Install", "i")
                .SetHelpDescription("Installs the supplied packages into the application, taking great care to do it properly!")
                .AddSwitch("Global", "g")
                .SetHelpDescription("Indicates that this should be installed globally so that everyone can access it and do exactly what they like with it without making sure it works!")
                .Command
                .Config
                .SetHelpExampleUsage("app.exe Install <package> --Global --Verbose");
            CliUtils.CreateParser(config).Parse("Help Help");
            var options = new HelpWriterOptions { LineWidth = 100 };
            using (var stream = new StreamWriter($"{this.GetAssemblyDir()}\\..\\..\\..\\Output\\{config.AppInfo.Name}-HelpCommandHelp.txt", false))
            {
                CliUtils.WriteHelp(config, stream, options);
            }
        }

    }
}
