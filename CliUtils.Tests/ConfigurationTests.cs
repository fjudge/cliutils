﻿using FJSolutions.CommandLine.Implementation;
using NUnit.Framework;
using System;
using System.Linq;

namespace FJSolutions.CommandLine.Tests
{
    [TestFixture]
    public class ConfigurationTests
    {
        [Test]
        public void TestCreatingBuilder()
        {
            var config = CliUtils.CreateConfiguration();

            Assert.That(config, Is.Not.Null);
            Assert.That(config.Global, Is.Not.Null);
            Assert.That(config.Global.Name, Is.Not.Empty);
        }


        [Test]
        public void TestAddingCommandSwitchWithoutName()
        {
            var config = CliUtils.CreateConfiguration();

            Assert.Throws<ArgumentException>(() => config.Global.AddSwitch(" "));
        }

        [Test]
        public void TestHelpCommand()
        {
            var config = (CliConfiguration)CliUtils.CreateConfiguration();

            Assert.That(config.Help, Is.Not.Null);
            Assert.That(config.Help.Name, Is.EqualTo("Help"));
            Assert.That(config.Help.ShortName, Is.EqualTo("?"));
        }


        [Test]
        public void TestAddingCommandSwitch()
        {
            var config = (CliConfiguration)CliUtils.CreateConfiguration();

            var switchConfig = config.Global.AddSwitch("--Verbose ");

            Assert.That(switchConfig, Is.Not.Null);
            Assert.That(switchConfig.Name, Is.Not.Empty);
            Assert.That(switchConfig.Name, Is.EqualTo("Verbose"));
            Assert.That(config.Global.Switches.Count(), Is.EqualTo(2));
        }

        [Test]
        public void TestSetttingSwitchShortName()
        {
            var config = CliUtils.CreateConfiguration();

            var switchConfig = config.Global.AddSwitch("--Verbose ", "v");

            Assert.That(switchConfig.ShortName, Is.EqualTo("v"));
        }


        [Test]
        public void TestSetttingSwitchHelpText()
        {
            var config = CliUtils.CreateConfiguration();
            var switchConfig = (SwitchConfig)config.Global.AddSwitch("Verbose")
                                    .SetHelpDescription("Short Help text")
                                    .SetHelpLongDescription("Long Help Text")
                                    .SetHelpExampleUsage("Example Text");

            Assert.That(switchConfig.HelpInfo.Description, Is.EqualTo("Short Help text"));
            Assert.That(switchConfig.HelpInfo.LongDescription[0], Is.EqualTo("Long Help Text"));
            Assert.That(switchConfig.HelpInfo.ExampleUsage, Is.EqualTo("Example Text"));
        }

        [Test]
        public void TestSetttingSwitchHasValue()
        {
            var config = CliUtils.CreateConfiguration();

            var switchConfig = (SwitchConfig)config.Global.AddSwitch("--Verbose");

            Assert.That(switchConfig.HasValue, Is.False);
            switchConfig.SetHasValue();
            Assert.That(switchConfig.HasValue, Is.True);
            switchConfig.SetHasValue(false);
            Assert.That(switchConfig.HasValue, Is.False);
        }

        [Test]
        public void TestrAddingCommand()
        {
            var config = (CliConfiguration)CliUtils.CreateConfiguration();
            var cmdConfig = config.AddCommand("Install");

            Assert.That(cmdConfig, Is.Not.Null);
            Assert.That(cmdConfig.Name, Is.EqualTo("Install"));
            Assert.That(config.Commands.Length, Is.EqualTo(1));
        }

        [Test]
        public void TestrAddingCommandEmptyNameThrows()
        {
            var config = CliUtils.CreateConfiguration();

            Assert.Throws<ArgumentException>(() => config.AddCommand(" "));
        }

        [Test]
        public void TestrAddingCommandWithShortName()
        {
            var config = CliUtils.CreateConfiguration();
            var cmdConfig = config.AddCommand("Install", "i");

            Assert.That(cmdConfig.ShortName, Is.EqualTo("i"));
        }

        [Test]
        public void TestrAddingCommandValueType()
        {
            var config = CliUtils.CreateConfiguration();
            var cmdConfig = config.AddCommand("Install");

            Assert.That(cmdConfig.CommandValueType, Is.EqualTo(CommandValueType.None));
            cmdConfig.SetValueType(CommandValueType.Multiple);
            Assert.That(cmdConfig.CommandValueType, Is.EqualTo(CommandValueType.Multiple));
        }

        [Test]
        public void TestSetttingCommandHelpText()
        {
            var config = (CommandConfig)CliUtils.CreateConfiguration()
                                    .Global
                                    .SetHelpDescription("Short Help text")
                                    .SetHelpLongDescription("Long Help Text")
                                    .SetHelpExampleUsage("Example Text");

            Assert.That(config.HelpInfo.Description, Is.EqualTo("Short Help text"));
            Assert.That(config.HelpInfo.LongDescription[0], Is.EqualTo("Long Help Text"));
            Assert.That(config.HelpInfo.ExampleUsage, Is.EqualTo("Example Text"));
        }

        [Test]
        public void TestSetttingAppInfoFromAssembly()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(CliUtils));

            Assert.That(config, Is.Not.Null);
            Assert.That(config.AppInfo, Is.Not.Null);
            Assert.That(config.AppInfo.Name, Is.Not.Empty);
            Assert.That(config.AppInfo.Company, Is.Not.Empty);
            Assert.That(config.AppInfo.Copyright, Is.Not.Empty);
            Assert.That(config.AppInfo.Description, Is.Not.Empty);
            Assert.That(config.AppInfo.VersionName, Is.Not.Empty);
            Assert.That(config.AppInfo.VersionNumber, Is.Not.Empty);
        }

        [Test]
        public void TestSetttingAppInfoFromEntryAssembly()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo();

            Assert.That(config, Is.Not.Null);
            Assert.That(config.AppInfo, Is.Not.Null);
            Assert.That(config.AppInfo.Name, Is.Not.Empty);
            Assert.That(config.AppInfo.Company, Is.Empty);
            Assert.That(config.AppInfo.Copyright, Is.Empty);
            Assert.That(config.AppInfo.Description, Is.Empty);
            Assert.That(config.AppInfo.VersionName, Is.Empty);
            Assert.That(config.AppInfo.VersionNumber, Is.Empty);
        }

        [Test]
        public void TestSetttingAppInfoFromAssemblyWithEmptyValues()
        {
            var config = CliUtils.CreateConfiguration()
                .SetAppInfo(typeof(ConfigurationTests));

            Assert.That(config, Is.Not.Null);
            Assert.That(config.AppInfo, Is.Not.Null);
            Assert.That(config.AppInfo.Name, Is.Not.Empty);
            Assert.That(config.AppInfo.Company, Is.Empty);
            Assert.That(config.AppInfo.Copyright, Is.Empty);
            Assert.That(config.AppInfo.Description, Is.Empty);
            Assert.That(config.AppInfo.VersionName, Is.Empty);
            Assert.That(config.AppInfo.VersionNumber, Is.Empty);
        }

        [Test]
        public void TestSetttingAppInfoFromAssemblyWithNullAssembly()
        {
            var config = CliUtils.CreateConfiguration();

            Assert.Throws<ArgumentException>(() => config.SetAppInfo(null));
        }
    }
}
