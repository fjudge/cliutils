﻿using NUnit.Framework;
using System;

namespace FJSolutions.CommandLine.Tests
{
    [TestFixture]
    public class UnconfiguredParserTests
    {
        [Test]
        public void TestGettingUnconfiguredParser()
        {
            ICliParser parser = CliUtils.CreateParser();

            Assert.That(parser, Is.Not.Null);

            IParseResult result = parser.Parse("New Application");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Command));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.HasBeenValidated, Is.False);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("New"));
            Assert.That(result.Arguments[0].HasValue, Is.True);
            Assert.That(result.Arguments[0].Value, Is.EqualTo("Application"));
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Command));
        }

        [Test]
        public void TestUnconfiguredParserValidationException1()
        {
            var parser = CliUtils.CreateParser();
            var result = parser.Parse("New Application");
            Assert.Throws<ArgumentException>(() => parser.Validate(result, null));
        }

        [Test]
        public void TestUnconfiguredParserValidationException()
        {
            var parser = CliUtils.CreateParser();
            Assert.Throws<ArgumentException>(() => parser.Validate(null, null));
        }
    }
}
