﻿using FJSolutions.CommandLine.Implementation;
using NUnit.Framework;
using System;

namespace FJSolutions.CommandLine.Tests
{
    [TestFixture]
    public class CliPasrserTests
    {
        private ICliConfiguration SetupConfig()
        {
            return CliUtils.CreateConfiguration()
                .Global
                .AddSwitch("Verbose", "V")
                .Command
                .Config;
        }

        private ICliParser SetupParser()
        {
            var config = this.SetupConfig();

            return CliUtils.CreateParser(config);
        }

        [Test]
        public void TestCreatingParser()
        {
            var parser = this.SetupParser() as CliParser;

            Assert.That(parser, Is.Not.Null);
        }

        [Test]
        public void TestParserTokenisingEmptyInput()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize(null);

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(0));
        }

        [Test]
        public void TestParserTokenisingSingleArgumentStringInput()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize("--verbose");

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(1));
            Assert.That(tokens[0], Is.EqualTo("--verbose"));
        }

        [Test]
        public void TestParserTokenisingSingleArgumentStringInputWithTrailingWhitespace()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize("--verbose ");

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(1));
            Assert.That(tokens[0], Is.EqualTo("--verbose"));
        }

        [Test]
        public void TestParserTokenisingSingleQuotedArgumentStringInput()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize(" \"--verbose\" ");

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(1));
            Assert.That(tokens[0], Is.EqualTo("--verbose"));
        }

        [Test]
        public void TestParserTokenisingSingleQuotedWhitespaceArgumentStringInput()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize(" \"Francis Judge\" ");

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(1));
            Assert.That(tokens[0], Is.EqualTo("Francis Judge"));
        }

        [Test]
        public void TestParserTokenisingMultipleArgumentStringInput()
        {
            var parser = (CliParser)this.SetupParser();

            var tokens = parser.Tokenize("CreateUser  --name \"Francis Judge\" ");

            Assert.That(tokens, Is.Not.Null);
            Assert.That(tokens.Length, Is.EqualTo(3));
            Assert.That(tokens[0], Is.EqualTo("CreateUser"));
            Assert.That(tokens[1], Is.EqualTo("--name"));
            Assert.That(tokens[2], Is.EqualTo("Francis Judge"));
        }

        [Test]
        public void TestEmptyCommandProcessing()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Empty));
            Assert.That(result.Arguments, Is.Empty);
        }

        [Test]
        public void TestHelpCommandProcessing()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("Help ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Help));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("Help"));
            Assert.That(result.Arguments[0].HasValue, Is.False);
        }

        [Test]
        public void TestHelpCommandProcessingWithValue()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("Help CommandName");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Help));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("Help"));
            Assert.That(result.Arguments[0].HasValue, Is.True);
            Assert.That(result.Arguments[0].Value, Is.EqualTo("CommandName"));
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Command));
            Assert.That(result.Arguments[0].Position, Is.EqualTo(0));
            Assert.That(result.Arguments[0].Raw, Is.EqualTo("Help"));
        }

        [Test]
        public void TestHelpShortCommandProcessingWithValue()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("-?");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Help));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("?"));
            Assert.That(result.Arguments[0].HasValue, Is.False);
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Switch));
            Assert.That(result.Arguments[0].Position, Is.EqualTo(0));
            Assert.That(result.Arguments[0].Raw, Is.EqualTo("-?"));
        }

        [Test]
        public void TestNoCommandCommandProcessing()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("--Verbose");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.SwitchesOnly));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("Verbose"));
            Assert.That(result.Arguments[0].HasValue, Is.False);
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Switch));
            Assert.That(result.Arguments[0].Position, Is.EqualTo(0));
            Assert.That(result.Arguments[0].Raw, Is.EqualTo("--Verbose"));
        }

        [Test]
        public void TestSwitchesOnlyShortNameCommandProcessing()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("-v");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.SwitchesOnly));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("v"));
            Assert.That(result.Arguments[0].HasValue, Is.False);
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Switch));
            Assert.That(result.Arguments[0].Position, Is.EqualTo(0));
            Assert.That(result.Arguments[0].Raw, Is.EqualTo("-v"));
        }

        [Test]
        public void TestCommandProcessingWithValue()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("New Application", false);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Command));
            Assert.That(result.Arguments, Is.Not.Empty);
            Assert.That(result.HasBeenValidated, Is.False);
            Assert.That(result.Arguments[0].Name, Is.EqualTo("New"));
            Assert.That(result.Arguments[0].HasValue, Is.True);
            Assert.That(result.Arguments[0].Value, Is.EqualTo("Application"));
            Assert.That(result.Arguments[0].ArgumentType, Is.EqualTo(CliArgumentType.Command));
        }

        [Test]
        public void TestCommandProcessingWithInvalidValue()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("New Application");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Invalid));
            Assert.That(result.ValidationResult.IsValid, Is.False);

            Console.WriteLine(result.ValidationResult.Messages[0]);
        }

        [Test]
        public void TestCommandProcessingWithValidSwitch()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("--Verbose");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.SwitchesOnly));
            Assert.That(result.ValidationResult.IsValid, Is.True);
        }

        [Test]
        public void TestCommandProcessingWithValidShortSwitch()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("-v");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.SwitchesOnly));
            Assert.That(result.ValidationResult.IsValid, Is.True);
        }

        [Test]
        public void TestCommandProcessingWithInvalidSwitch()
        {
            var parser = this.SetupParser();
            var result = parser.Parse("--NONEXISTENT");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Invalid));
            Assert.That(result.ValidationResult.IsValid, Is.False);

            Console.WriteLine(result.ValidationResult.Messages[0]);
        }

        [Test]
        public void TestValidationOfMissingCommandSwitch()
        {
            var config = this.SetupConfig()
                .AddCommand("New")
                .Config;
            var parser = CliUtils.CreateParser(config);
            var result = parser.Parse("New \"Francis Judge\" --Verbose --NO_SWITCH");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Invalid));
            Assert.That(result.ValidationResult.IsValid, Is.False);

            Console.WriteLine(result.ValidationResult.Messages[0]);
        }

        [Test]
        public void TestValidationOfCommandSwitch()
        {
            var config = this.SetupConfig()
                .AddCommand("New")
                .AddSwitch("Create")
                .Command
                .Config;
            var parser = CliUtils.CreateParser(config);
            var result = parser.Parse("New \"Francis Judge\" --Create");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Command));
            Assert.That(result.ValidationResult.IsValid, Is.True);
        }

        [Test]
        public void TestValidationOfInvalidCommandSwitch()
        {
            var config = this.SetupConfig()
                .AddCommand("New")
                .AddSwitch("Create")
                .Command
                .Config;
            var parser = CliUtils.CreateParser(config);
            var result = parser.Parse("New \"Francis Judge\" --Create --NON_EXISTENT ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Invalid));
            Assert.That(result.ValidationResult.IsValid, Is.False);

            Console.WriteLine(result.ValidationResult.Messages[0]);
        }

        [Test]
        public void TestValidationOfInvalidCommandName()
        {
            var config = this.SetupConfig()
                .AddCommand("New")
                .AddSwitch("Create")
                .Command
                .Config;
            var parser = CliUtils.CreateParser(config);
            var result = parser.Parse("NON_EXISTENT ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.HasBeenValidated, Is.True);
            Assert.That(result.ResultType, Is.EqualTo(ParseResultType.Invalid));
            Assert.That(result.ValidationResult.IsValid, Is.False);

            Console.WriteLine(result.ValidationResult.Messages[0]);
        }

        [Test]
        public void TestCommandAndSwitchNavigationByIndexer()
        {
            var config = (CliConfiguration)this.SetupConfig()
                .AddCommand("New")
                .AddSwitch("Create")
                .Command
                .Config;
            var cmd = (CommandConfig)config["New"];
            Assert.That(cmd, Is.Not.Null);
            Assert.That(cmd.Name, Is.EqualTo("New"));
            var sw = cmd["Create"];
            Assert.That(sw, Is.Not.Null);
            Assert.That(sw.Name, Is.EqualTo("Create"));
        }


    }
}
